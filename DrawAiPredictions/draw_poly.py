import ROOT
import math
from math import cos,sin
from tqdm import tqdm



# function for calculating partial polynomials
#--------------------------------------------------------------------------
def polynAi(index, cosThetaCS, phiCS):
    temp = 1
    m_nAi = 8
    sinThetaCS = 0
    if 1.0 - cosThetaCS * cosThetaCS > 0: sinThetaCS = (1.0 - cosThetaCS * cosThetaCS)**0.5
    sin2ThetaCS = 2 * sinThetaCS * cosThetaCS

    if index == m_nAi:
        temp = 1 + cosThetaCS * cosThetaCS
    elif index == 0:
        temp = 0.5 * (1. - 3. * cosThetaCS * cosThetaCS)
    elif index == 1:
        temp = sin2ThetaCS * cos(phiCS)
    elif index == 2:
        temp = 0.5 * sinThetaCS * sinThetaCS * cos(2 * phiCS);
    elif index == 3:
        temp = sinThetaCS * cos(phiCS)
    elif index == 4:
        temp = cosThetaCS
    elif index == 5:
        temp = sinThetaCS * sinThetaCS * sin(2 * phiCS)
    elif index == 6:
        temp = sin2ThetaCS * sin(phiCS)
    elif index == 7:
        temp = sinThetaCS * sin(phiCS)

    return temp

def main():
    f = ROOT.TFile("/eos/user/d/dponomar/Storage/Science/Wai/microtree_example/PDF_reweight/v20220718/mc16_13TeV.361100.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusenu.e3601_s3126_r10244+r11165_p3665.root")
    t = f.Get("MicroTree/HWWTree_ee")
    evt = t.GetEntries()
    hist_folder = {}
    for Ai in range(8):
        hist_folder[Ai] = ROOT.TH2D("h%s"%Ai,";#phi_{CS}; cos(#theta_{CS});P_{%s}(#phi_{CS},cos(#theta_{CS}))"%Ai, 50,0, 2*math.pi, 50, 0,1)

    for i in tqdm(range(2000000)):
        t.GetEntry(i)
        cosTheta = 0 
        phi = 0
        cosTheta = t.CosThetaCS_truth
        phi = t.PhiCS_truth 
        for Ai in range(8):
            w_0 = 0
            w_0 = polynAi(Ai,cosTheta,phi )
            hist_folder[Ai].Fill(phi,cosTheta, w_0)

    for Ai in range(8):
        c = ROOT.TCanvas('c%s'%Ai,'c%s'%Ai,600,600)
        ROOT.gStyle.SetOptStat(0)
        hist_folder[Ai].Draw("colz")
        hist_folder[Ai].GetZaxis().SetLabelOffset(999)
        hist_folder[Ai].GetZaxis().SetTitleOffset(0.5)
        hist_folder[Ai].GetZaxis().SetLabelSize(0)
        c.Draw()
        c.SaveAs("plot_poly/"+"poly_"+str(Ai)+".pdf")
        c.SaveAs("plot_poly/"+"poly_"+str(Ai)+".png")


if __name__ == "__main__":
    main()