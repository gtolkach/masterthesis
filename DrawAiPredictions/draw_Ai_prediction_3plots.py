import ROOT

from filereader.filereader import CalcArefAndUns,readArefAndUnsFromFile,fillAiHist,fillAiHist_v1,CalcAiDiffHist_v1

def DrawAiPrediction_3plots(hist, hist_diff,hist_pdf_err, var, save_dir, channel, Ai,version,logXex=False ):
    c3 =  ROOT.TCanvas("c3", "ph_et in", 0,0,600,600)
    pad_1 = ROOT.TPad("pad1","This is pad1", 0.01, 0.6, 1, 1.0)
    pad_2 = ROOT.TPad("pad2","This is pad2", 0.01, 0.40, 1, 0.59)
    pad_3 = ROOT.TPad("pad3","This is pad3", 0.01, 0.001, 1, 0.39)
    pad_1.SetBorderSize(0)
    pad_1.SetBottomMargin(0.0)
    pad_1.SetLeftMargin(0.12)
    pad_1.SetRightMargin(1.0)
    pad_1.SetBottomMargin(0.0)
    pad_1.Draw()
    pad_2.SetBorderSize(0)
    pad_2.SetTopMargin(0.)
    pad_2.SetLeftMargin(0.12)
    pad_2.SetRightMargin(1.0)
    pad_2.SetBottomMargin(0.0)
    pad_2.Draw()
    
    pad_3.SetBorderSize(0)
    pad_3.SetTopMargin(0.)
    pad_3.SetLeftMargin(0.12)
    pad_3.SetRightMargin(1.0)
    pad_3.SetBottomMargin(0.2)

    pad_3.Draw()

    hist_name = [ "CT10","CT18NNLO", "MSHT20NNLO", "NNPDF4.0NNLO"]

    #hist = {hist_name[0]:h_m, hist_name[1]:h_CT10, hist_name[2]:h_CT18, hist_name[3] :h_MSHT ,hist_name[4]: h_NNPDF}
    #color_list = {hist_name[0]: 1,hist_name[1]:209, hist_name[2]: 2, hist_name[3]:ROOT.kPink+8, hist_name[4]:ROOT.kAzure+1,  }
    #color_list = {hist_name[0]: ROOT.kOrange-3 , hist_name[1]:2,hist_name[3]:ROOT.kAzure+1, hist_name[2]: ROOT.kGreen+3}
    MarketStyle_list = {hist_name[0]:25, hist_name[1]:32, hist_name[3]: 4,  hist_name[2]:31}
    
    color_list = {hist_name[0]:209, hist_name[1]: 2, hist_name[2]:ROOT.kOrange+7, hist_name[3]:ROOT.kAzure+1,  }
    hist_name_3 = [ "pdf unc. from CT10", "pdf unc. from CT18NNLO", "pdf unc. from MSHT20NNLO", "pdf unc. from NNPDF4.0NNLO", "total measurement unc."]
    #color_list_3 = {hist_name_3[0]: 1, hist_name_3[1]: ROOT.kOrange-3, hist_name_3[2]: 2, hist_name_3[3]: 31, hist_name_3[4]: 4,  }
    color_list_3 = {hist_name_3[4]: 1,hist_name_3[0]:209, hist_name_3[1]: 2, hist_name_3[2]:ROOT.kOrange+7, hist_name_3[3]:ROOT.kAzure+1}



        
    legend = ROOT.TLegend(0.15, 0.5,0.5,0.85,"", "NDC")
    frac_insert = 0.3 + 0.94 - 0.87
    header = ""
    header = "Powheg+Pythia8,"
    if "WMenu" in channel:
        header += " W^{-} #rightarrow e^{-} #nu"
    elif "WPenu" in channel:
        header += " W^{+} #rightarrow e^{+} #nu"
        print("+")

    legend.SetHeader(header)

    legend.SetFillStyle(0)
    legend.SetTextSize(0.06)
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetTextFont(42)


    legend_3 = ROOT.TLegend(0.15, 0.65,0.35,0.98,"", "NDC")
    legend_3.SetFillStyle(0)
    legend_3.SetTextSize(0.06)
    legend_3.SetBorderSize(0)
    legend_3.SetFillColor(0)
    legend_3.SetTextFont(42)


    mg = ROOT.TMultiGraph()
    mg_2 = ROOT.TMultiGraph()
    mg_3 = ROOT.TMultiGraph()


    pad_1.SetTickx()
    pad_1.SetTicky()
    pad_2.SetTickx()
    pad_2.SetTicky()
    pad_3.SetTickx()
    pad_3.SetTicky()
    ROOT.gStyle.SetEndErrorSize(7)


    for pdf in hist_name:

        hist[pdf].SetTitle("")
        hist[pdf].SetMarkerColor(color_list[pdf])
        hist[pdf].SetLineColor(color_list[pdf])
        hist[pdf].SetMarkerStyle(MarketStyle_list[pdf])
        hist[pdf].SetMarkerSize(1.5)
        hist[pdf].SetLineWidth(2)

            #hist[pdf].SetFillStyle(3004)
            #hist[pdf].SetFillColor(color_list[pdf])


        mg.Add(hist[pdf])
        legend.AddEntry(hist[pdf], pdf)

        hist_diff[pdf].SetTitle("")
        hist_diff[pdf].SetMarkerColor(color_list[pdf])
        hist_diff[pdf].SetLineColor(color_list[pdf])
        hist_diff[pdf].SetMarkerStyle(MarketStyle_list[pdf])
        hist_diff[pdf].SetMarkerSize(1.5)
        hist_diff[pdf].SetLineWidth(2)

        #if "CT18" in pdf:
        #    hist_diff[pdf].SetFillColorAlpha(color_list[pdf],0.3)
        #else:
        #    hist_diff[pdf].SetFillColor(color_list[pdf])
        #    #hist_diff[pdf].SetFillColor(color_list[pdf])
        #    hist_diff[pdf].SetFillStyle(MarketFill_list[pdf])
        if "CT10" not in pdf:
            mg_2.Add(hist_diff[pdf])
        hist[pdf].SetTitle("")

    for pdf in hist_name_3:
        hist_pdf_err[pdf].SetLineColor(1)
        if "CT10" in pdf: continue
        if "total" in pdf:
            #hist_pdf_err[pdf].SetFillColorAlpha(color_list_3[pdf],0.1)
            hist_pdf_err[pdf].SetFillColor(color_list_3[pdf])
            hist_pdf_err[pdf].SetFillStyle(3345)
            mg_3.Add(hist_pdf_err[pdf])

        else:
            tmp = hist_pdf_err[pdf].Clone("tmpCLONE"+pdf)
            tmp.SetFillColor(ROOT.kWhite)
            mg_3.Add(tmp)
            hist_pdf_err[pdf].SetFillColorAlpha(color_list_3[pdf],0.6)
            mg_3.Add(hist_pdf_err[pdf])


        legend_3.AddEntry(hist_pdf_err[pdf], pdf,"f")


    pad_1.cd(0)
    mg.Draw("apz")
    #mg.Draw("pX")

    ROOT.gPad.RedrawAxis()
    c3.Update()
    gr1 = ROOT.TGraph(2)
    gr1.SetPoint(0, -1, 0)
    gr1.SetPoint(1, 1000, 0)
    gr1.SetLineStyle(7)
    ROOT.gPad.RedrawAxis()
    gr1.Draw("same")
    legend.Draw("same")
    mg_2.GetYaxis().SetRangeUser(-0.07, 0.07)
   
    print(var)



    if var == "y":
        mg_3.GetXaxis().SetTitle("|y^{l#nu}|")

        ymax = mg.GetYaxis().GetXmax()
        ymin = min(-0.01, mg.GetYaxis().GetXmin())
        dy0 = ymax - ymin
        dy1 = frac_insert / (1 - frac_insert) * dy0
        dy_tot = dy1 + dy0
        mg.GetYaxis().SetRangeUser(ymin, ymin + dy_tot)
        mg_2.GetYaxis().SetRangeUser(-0.031, 0.031)
        if Ai in ["4"]:
            if "WPenu" in channel:
                mg_2.GetYaxis().SetRangeUser(-0.07, 0.07)
            elif "WMenu" in channel:
                mg_2.GetYaxis().SetRangeUser(-0.05, 0.10)
            mg_3.GetYaxis().SetRangeUser(-0.05, 0.3)
        elif Ai in "CS":
            #mg_2.GetYaxis().SetRangeUser(-250, 50)
            mg_2.GetYaxis().SetRangeUser(0.93, 1.15)
            mg_3.GetYaxis().SetRangeUser(-150, 500)
            mg.GetYaxis().SetRangeUser(1000, 5000)
            if "WPenu" in channel:
                mg.GetYaxis().SetRangeUser(2000, 5500)
            #if "WPenu" in channel:
            #    mg_2.GetYaxis().SetRangeUser(-1500, 500)


    if Ai not in "CS":    
        mg.GetYaxis().SetTitle("A_{%s}"%Ai)
        mg.GetYaxis().SetLabelSize(0.07)
        mg.GetYaxis().SetTitleSize(0.075)
        mg.GetYaxis().SetTitleOffset(0.70)
        mg_2.GetYaxis().SetTitle("A_{%s}(CT10)-A_{%s}"%(Ai,Ai))
        mg_3.GetYaxis().SetTitle("Uncertainty in A_{%s}"%Ai)


    else:
        if var in "y":
            mg.GetYaxis().SetTitle("d#sigma/dy")
            mg_2.GetYaxis().SetTitle("#sigma/#sigma(CT10)")

        if var in "pt":
            mg.GetYaxis().SetTitle("d#sigma/dp_{T}")
            mg_2.GetYaxis().SetTitle("#sigma/#sigma(CT10)")
        
        mg_3.GetYaxis().SetTitle("Uncertainty in d#sigma/dy")
        #mg.GetYaxis().SetRangeUser()

        mg.GetYaxis().SetLabelOffset(0.0)
        mg.GetXaxis().SetMoreLogLabels(0)
        mg.GetXaxis().SetNoExponent(0)

        mg.GetYaxis().SetLabelSize(0.07)
        mg.GetYaxis().SetTitleSize(0.075)
        mg.GetYaxis().SetTitleOffset(0.8)


    #mg_3.GetYaxis().SetTitleSize(0.13)
    #mg_3.GetYaxis().SetTitleOffset(0.37)


    mg_3.GetYaxis().SetLabelSize(0.07)
    mg_3.GetYaxis().SetTitleSize(0.08)
    mg_3.GetYaxis().SetTitleOffset(0.7)


    mg_3.GetXaxis().SetLabelSize(0.08)
    mg_3.GetXaxis().SetTitleSize(0.084)


    mg_2.GetYaxis().SetLabelSize(0.13)
    mg_2.GetYaxis().SetTitleSize(0.15)
    mg_2.GetYaxis().SetTitleOffset(0.37)

    pad_2.cd(0)
    mg_2.Draw("apz")
    gr2 = ROOT.TGraph(2)
    if Ai not in "CS":   
        gr2.SetPoint(0, -1, 0)
        gr2.SetPoint(1, 1000, 0)
    else:   
        gr2.SetPoint(0, -1, 1)
        gr2.SetPoint(1, 1000, 1)
    gr2.SetLineStyle(7)
    ROOT.gPad.RedrawAxis()
    gr2.Draw("same")
    
    pad_3.cd(0)
    mg_3.Draw("a2")
    legend_3.Draw("same")


    if logXex:
        pad_1.SetLogx()
        pad_2.SetLogx()
        pad_3.SetLogx()

    pad_1.Update()
    pad_2.Update()
    pad_3.Update()
    pad_1.RedrawAxis()
    pad_2.RedrawAxis()
    pad_3.RedrawAxis()

    c3.Draw()
    save_name = Ai+"_"+var+"_"+channel
    save_dir = "plots"
    c3.SaveAs(save_dir+"/"+save_name+".pdf")
    c3.SaveAs(save_dir+"/"+save_name+".png")
    #c3.SaveAs("testplot.png")

if __name__ == "__main__":

    Ai_n = 8
    var = "y"
    save_dir = "plotsAref/prediction/CT10_CT18_NNPDF40/y"
    channel = "WMenu"
    logXex = False
    version = "v1.0"
    Ai = "CrossSection"
    #Ai = "A4"
        

    #eminus 
    #y
    #file_path = "/eos/user/g/gtolkach/WAi/test_123/WMenu_WAi_y_CT10NLO_v1.0_CC_v1.0/pseudoData/pt00_m00/TOT.txt" 
    #total unc. from ALEX
    #file_path = "/eos/user/g/gtolkach/WAi_unc/WMenu_TOT.txt"
    file_path = "/afs/cern.ch/work/g/gtolkach/git/forGrigorii/asimov/WMenu_y/TOT.txt"

    files_path_CT18NNLO = "/eos/user/g/gtolkach/WAi/prediction/CC_WMenu_WAi_y_CT18NNLO_v1.0/mainScanTruth/"
    files_path_CT10NLO = "/eos/user/g/gtolkach/WAi/prediction/CC_WMenu_WAi_y_CT10NLO_v1.0/mainScanTruth/"
    files_path_NNPDF = "/eos/user/g/gtolkach/WAi/prediction/CC_WMenu_WAi_y_NNPDF40_v1.0/mainScanTruth/"
    files_path_MSHT = "/eos/user/g/gtolkach/WAi/prediction/CC_WMenu_WAi_y_MSHT_v1.0/mainScanTruth/"


    #eplus
    #total unc. from ALEX
    #file_path = "/eos/user/g/gtolkach/WAi/test_123/WPenu_WAi_y_CT10NLO_v1.0_CC_v4.0/pseudoData/pt00_m00/TOT.txt" 
    #file_path = "/eos/user/g/gtolkach/WAi_unc/WPenu_TOT.txt"
    #files_path_CT18NNLO = "/eos/user/g/gtolkach/WAi/prediction/CC_WPenu_WAi_y_CT18NNLO_v1.0/mainScanTruth/"
    #files_path_CT10NLO = "/eos/user/g/gtolkach/WAi/prediction/CC_WPenu_WAi_y_CT10NLO_v1.0/mainScanTruth/"
    #files_path_NNPDF = "/eos/user/g/gtolkach/WAi/prediction/CC_WPenu_WAi_y_NNPDF40_v1.0/mainScanTruth/"
    #files_path_MSHT = "/eos/user/g/gtolkach/WAi/prediction/CC_WPenu_WAi_y_MSHT_v1.0/mainScanTruth/"



    if "CrossSection" in Ai:
        graph_from_file = readArefAndUnsFromFile(9,file_path, "y")
    else:
        graph_from_file = readArefAndUnsFromFile(4,file_path, "y")
        
    AiRef_CT18, AiRefErr_CT18, pdf_err_hi_CT18, pdf_err_lo_CT18,  pdf_scale_err_hi_CT18, pdf_scale_err_lo_CT18  = CalcArefAndUns(files_path_CT18NNLO)
    AiRef_CT10, AiRefErr_CT10, pdf_err_hi_CT10, pdf_err_lo_CT10,  pdf_scale_err_hi_CT10, pdf_scale_err_lo_CT10   = CalcArefAndUns(files_path_CT10NLO)
    AiRef_NNPDF, AiRefErr_NNPDF, pdf_err_hi_NNPDF, pdf_err_lo_NNPDF,  pdf_scale_err_hi_NNPDF, pdf_scale_err_lo_NNPDF   = CalcArefAndUns(files_path_NNPDF)
    AiRef_MSHT, AiRefErr_MSHT, pdf_err_hi_MSHT, pdf_err_lo_MSHT,  pdf_scale_err_hi_MSHT, pdf_scale_err_lo_MSHT   = CalcArefAndUns(files_path_MSHT)


    AiRef_CT18, AiRefErr_CT18, pdf_err_hi_CT18, pdf_err_lo_CT18,  pdf_scale_err_hi_CT18, pdf_scale_err_lo_CT18  = CalcArefAndUns(files_path_CT18NNLO)
    AiRef_CT10, AiRefErr_CT10, pdf_err_hi_CT10, pdf_err_lo_CT10,  pdf_scale_err_hi_CT10, pdf_scale_err_lo_CT10  = CalcArefAndUns(files_path_CT10NLO)
    AiRef_NNPDF, AiRefErr_NNPDF, pdf_err_hi_NNPDF, pdf_err_lo_NNPDF,  pdf_scale_err_hi_NNPDF, pdf_scale_err_lo_NNPDF  = CalcArefAndUns(files_path_NNPDF)
    AiRef_MSHT, AiRefErr_MSHT, pdf_err_hi_MSHT, pdf_err_lo_MSHT,  pdf_scale_err_hi_MSHT, pdf_scale_err_lo_MSHT  = CalcArefAndUns(files_path_MSHT)

    hist_Ai_graph_CT10, _, _, _, _, hist_Ai_pdf_total_CT10 = fillAiHist_v1(AiRef_CT10, AiRefErr_CT10,
                                                                            pdf_err_hi_CT10, pdf_err_lo_CT10,
                                                                            pdf_scale_err_hi_CT10, pdf_scale_err_lo_CT10,var)
    hist_Ai_graph_CT18, _, _, _, _,hist_Ai_pdf_total_CT18 = fillAiHist_v1(AiRef_CT18, AiRefErr_CT18,
                                                                            pdf_err_hi_CT18, pdf_err_lo_CT18,
                                                                            pdf_scale_err_hi_CT18, pdf_scale_err_lo_CT18,var)

    hist_Ai_graph_NNPDF, _, _, _, _,hist_Ai_pdf_total_NNPDF = fillAiHist_v1(AiRef_NNPDF, AiRefErr_NNPDF,
                                                                            pdf_err_hi_NNPDF, pdf_err_lo_NNPDF,
                                                                            pdf_scale_err_hi_NNPDF, pdf_scale_err_lo_NNPDF,var)
    
    hist_Ai_graph_MSHT, _, _, _, _,hist_Ai_pdf_total_MSHT = fillAiHist_v1(AiRef_MSHT, AiRefErr_MSHT,
                                                                       pdf_err_hi_MSHT, pdf_err_lo_MSHT,
                                                                       pdf_scale_err_hi_MSHT, pdf_scale_err_lo_MSHT,var)

    graph_CT10_CT18, stat__diff_CT10_CT18, pdf__diff_CT10_CT18, scale__diff_CT10_CT18, total__diff_CT10_CT18 = CalcAiDiffHist_v1(AiRef_CT10, AiRefErr_CT10,
                                                                                                                pdf_err_hi_CT10, pdf_err_lo_CT10,
                                                                                                                pdf_scale_err_hi_CT10, pdf_scale_err_lo_CT10,
                                                                                                                AiRef_CT18, AiRefErr_CT18,
                                                                                                                pdf_err_hi_CT18, pdf_err_lo_CT18,
                                                                                                                pdf_scale_err_hi_CT18, pdf_scale_err_lo_CT18,var)

    graph_CT10_CT10, stat__diff_CT10_CT10, pdf__diff_CT10_CT10, scale__diff_CT10_CT10, total__diff_CT10_CT10 = CalcAiDiffHist_v1(AiRef_CT10, AiRefErr_CT10,
                                                                                                                pdf_err_hi_CT10, pdf_err_lo_CT10,
                                                                                                                pdf_scale_err_hi_CT10, pdf_scale_err_lo_CT10,
                                                                                                                AiRef_CT10, AiRefErr_CT10,
                                                                                                                pdf_err_hi_CT10, pdf_err_lo_CT10,
                                                                                                                pdf_scale_err_hi_CT10, pdf_scale_err_lo_CT10,var)


    graph_CT10_NNPDF, stat__diff_CT10_NNPDF, pdf__diff_CT10_NNPDF, scale__diff_CT10_NNPDF, total__diff_CT10_NNPDF = CalcAiDiffHist_v1(AiRef_CT10, AiRefErr_CT10,
                                                                                                                pdf_err_hi_CT10, pdf_err_lo_CT10,
                                                                                                                pdf_scale_err_hi_CT10, pdf_scale_err_lo_CT10,
                                                                                                                AiRef_NNPDF, AiRefErr_NNPDF,
                                                                                                                pdf_err_hi_NNPDF, pdf_err_lo_NNPDF,
                                                                                                                pdf_scale_err_hi_NNPDF, pdf_scale_err_lo_NNPDF,var,num=1)

    graph_CT10_MSHT, stat__diff_CT10_MSHT, pdf__diff_CT10_MSHT, scale__diff_CT10_MSHT, total__diff_CT10_MSHT = CalcAiDiffHist_v1(AiRef_CT10, AiRefErr_CT10,
                                                                                                                pdf_err_hi_CT10, pdf_err_lo_CT10,
                                                                                                                pdf_scale_err_hi_CT10, pdf_scale_err_lo_CT10,
                                                                                                                AiRef_MSHT, AiRefErr_MSHT,
                                                                                                                pdf_err_hi_MSHT, pdf_err_lo_MSHT,
                                                                                                                pdf_scale_err_hi_MSHT, pdf_scale_err_lo_MSHT,var,num=2)


    hist = {"CT10": hist_Ai_graph_CT10[Ai].Clone("CT10"),
            "CT18NNLO": hist_Ai_graph_CT18[Ai].Clone("CT18"),
            "NNPDF4.0NNLO": hist_Ai_graph_NNPDF[Ai].Clone("NNPDF"),
            "MSHT20NNLO" : hist_Ai_graph_MSHT[Ai].Clone("MSHT") }
    hist_diff = {"CT10" : graph_CT10_CT10[Ai].Clone("CT10CT10"),
                "CT18NNLO": graph_CT10_CT18[Ai].Clone("CT18"),
                "NNPDF4.0NNLO": graph_CT10_NNPDF[Ai].Clone("CT10NNPDF"),
                "MSHT20NNLO": graph_CT10_MSHT[Ai].Clone("MSHT")}

    hist_pdf_err = {"total measurement unc.": graph_from_file.Clone("m"),
                    "pdf unc. from CT10" : hist_Ai_pdf_total_CT10[Ai].Clone("CT10err"),
                    "pdf unc. from CT18NNLO": hist_Ai_pdf_total_CT18[Ai].Clone("CT18err"),
                    "pdf unc. from NNPDF4.0NNLO": hist_Ai_pdf_total_NNPDF[Ai].Clone("NNPDFerr"),
                    "pdf unc. from MSHT20NNLO" : hist_Ai_pdf_total_MSHT[Ai].Clone("MSHTerr") }
    if "CrossSection" in Ai:
            DrawAiPrediction_3plots(hist, hist_diff, hist_pdf_err,var, save_dir, channel, "CS",version,logXex )
    else:
        DrawAiPrediction_3plots(hist, hist_diff, hist_pdf_err,var, save_dir, channel, Ai.split("A")[1],version,logXex )
        #else:
        #    hist = {"CT10" : hist_Ai_graph_CT10["CrossSection"].Clone("CT10"),
        #            "CT18NNLO": hist_Ai_graph_CT18["CrossSection"].Clone("CT18"),
        #            "NNPDF4.0NNLO": hist_Ai_graph_NNPDF["CrossSection"].Clone("NNPDF"),
        #            "MSHT20NNLO" : hist_Ai_graph_MSHT["CrossSection"].Clone("MSHT") }
        #    hist_diff = {"CT10" : graph_CT10_CT10["CrossSection"].Clone("CT10CT10"),
        #                "CT18NNLO": graph_CT10_CT18["CrossSection"].Clone("CT18"),
        #                "NNPDF4.0NNLO": graph_CT10_NNPDF["CrossSection"].Clone("CT10NNPDF"),
        #                "MSHT20NNLO": graph_CT10_MSHT["CrossSection"].Clone("MSHT")}
        #    DrawAiPrediction_3plots(hist, hist_diff, var, save_dir, channel, "CS",version,logXex )