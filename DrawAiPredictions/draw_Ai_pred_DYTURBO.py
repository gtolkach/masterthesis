import ROOT

from filereader.filereader import readAiFromDYTURBOfiles, CalcArefAndUns,fillAiHist


file_path_LO = "/afs/cern.ch/work/g/gtolkach/application/masterthesis/datafiles/dyturbo_output/results_A4_LO.txt"
file_path_NLO = "/afs/cern.ch/work/g/gtolkach/application/masterthesis/datafiles/dyturbo_output/results_A4_NLO.txt"
file_path_NNLO = "/afs/cern.ch/work/g/gtolkach/application/masterthesis/datafiles/dyturbo_output/results_A4_NNLO.txt"
file_path_N3LO = "/afs/cern.ch/work/g/gtolkach/application/masterthesis/datafiles/dyturbo_output/results_A4_N3LO.txt"
files_path_PP8 = "/afs/cern.ch/work/g/gtolkach/application/masterthesis/ConvertScripts/output/452023_12_30_47/"

h_LO = readAiFromDYTURBOfiles("4",file_path_LO, "pt")
h_NLO = readAiFromDYTURBOfiles("4",file_path_NLO, "pt")
h_NNLO = readAiFromDYTURBOfiles("4",file_path_NNLO, "pt")
h_N3LO = readAiFromDYTURBOfiles("4",file_path_N3LO, "pt")

var = "pt2"
AiRef, AiRefErr, pdf_err_hi, pdf_err_lo,  pdf_scale_err_hi, pdf_scale_err_lo  = CalcArefAndUns(files_path_PP8)
hist_Ai_graph, hist_Ai_stat_graph, hist_Ai_pdf_graph, hist_Ai_scale_graph, hist_Ai_total_graph, _ = fillAiHist(AiRef, AiRefErr,
                                                                                                                pdf_err_hi, pdf_err_lo,
                                                                                                                pdf_scale_err_hi, pdf_scale_err_lo,
                                                                                                                var)

c = ROOT.TCanvas()
#hist_Ai_graph["A4"].SetLineColor(5)
#hist_Ai_graph["A4"].Draw("azp")
legend = ROOT.TLegend(0.15, 0.1, 0.5,0.25,"", "NDC")
legend.SetFillStyle(0)
legend.SetTextSize(0.03)
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetTextFont(42)
legend.SetHeader("DYTURBO")

legend.AddEntry(h_LO, "LO")
legend.AddEntry(h_NLO, "NLO")
legend.AddEntry(h_NNLO, "NNLO")
legend.AddEntry(h_N3LO, "N3LO")

h_LO.GetYaxis().SetRangeUser(0.45, 0.95)

h_LO.Draw("hist")
h_NLO.SetLineColor(2)
h_NLO.Draw("same hist")
h_NNLO.SetLineColor(3)
h_NNLO.Draw("same hist")
h_N3LO.SetLineColor(1)
h_N3LO.Draw("same hist")
legend.Draw("same")

c.Draw()
c.SaveAs("plots/dyturbo/test.png")