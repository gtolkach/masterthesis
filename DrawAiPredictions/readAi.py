from filereader.filereader import CalcArefAndUns, PrintArefAndUnc

def main():
    file_path = "/afs/cern.ch/work/g/gtolkach/application/masterthesis/ConvertScripts/output/lowest_order_wpluesenu_MG/"
    AiRef, AiRefErr, pdf_err_hi, pdf_err_lo,  pdf_scale_err_hi, pdf_scale_err_lo  = CalcArefAndUns(file_path)
    PrintArefAndUnc(AiRef, AiRefErr, pdf_err_hi, pdf_err_lo,  pdf_scale_err_hi, pdf_scale_err_lo)
if __name__ == "__main__":
    main()