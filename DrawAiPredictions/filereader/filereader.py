import ROOT
import os
from math import sqrt
from array import array

Ai_n = 8
def GetAref(hist, hists_wi, hists_wiwi):
    AiRef = {}
    AiRefErr = {}
    scales= [20./3, 5, 20, 4, 4, 5, 5, 4]
    CrossSection = []
    CrossSection_err = []
    lumu = 335.18
    var_flag = 0
    for Ai in range(Ai_n):
        AiRef_list = []
        AiRefErr_list = []
        for i in range(hists_wi[Ai].GetNbinsX()):
            #M
            for j in range(hists_wi[Ai].GetNbinsY()):
                for k in range(hists_wi[Ai].GetNbinsZ()):
                    if i>1: var_flag = 1
                    elif k>1:  var_flag = 2
                    den = hist.GetBinContent(i + 1, j + 1, k + 1)
                    num = hists_wi[Ai].GetBinContent(i + 1, j + 1, k + 1)
                    content = num / den * scales[Ai] if den > 0 else 0
                    if Ai == 0 and den > 0:
                        content += 2. / 3
                    elif (Ai == 3 or Ai == 1) and den > 0:
                        content *=-1
                    AiRef_list.append(content)

                    norm = den
                    err = num
                    if err == 0.0:
                        n_eff = 0.0
                    else:    
                        n_eff = pow(norm / err, 2)
                    e_x2 = hists_wiwi[Ai].GetBinContent(i + 1, j + 1, k + 1) /norm if norm > 0  else 0
                    e_x_2 =  pow(hists_wi[Ai].GetBinContent(i + 1, j + 1, k + 1) / norm, 2) if norm > 0 else 0
                    sigma = sqrt(abs(e_x2 - e_x_2))* scales[Ai]  if norm > 0 else 0
                    if norm != 0.0:
                        deltaAi = sigma/sqrt(norm)
                    else:
                        deltaAi = 0
                    AiRefErr_list.append(deltaAi)
                    if Ai == Ai_n-1:
                        tmp_hist = ''
                        N_bin = 0
                        if var_flag == 1:
                            tmp_hist = hist.ProjectionX()
                            N_bin = i+1
                        elif var_flag == 2:
                            tmp_hist = hist.ProjectionZ()
                            N_bin = k+1
                            
                        normalisation = round(tmp_hist.GetBinWidth(N_bin),1) 
                        CrrSec_value = den/(lumu*normalisation)
                        if den != 0:
                            CrrSec_err = 1/sqrt(den) 
                        else:
                            CrrSec_err = 0
                        CrossSection.append(CrrSec_value)
                        CrossSection_err.append(CrrSec_err*CrrSec_err)
                        
                    #if Ai == 0:
                         #print("A%s"%Ai,"pt bin",i,"sqrt(abs(e_x2 - e_x_2))/(norm)* scales[Ai]:", deltaAi,"sqrt(abs(e_x2 - e_x_2))/sqrt(n_eff)* scales[Ai]:" ,sqrt(abs(e_x2 - e_x_2))/sqrt(n_eff)* scales[Ai])
                     #    print("A%s"%Ai,"ibin %s"%(i+1),j+1,k+1,content,"+-%s"%deltaAi, n_eff)
        
        AiRefErr["A%s"%Ai] = AiRefErr_list
        AiRef["A%s"%Ai] = AiRef_list
    AiRef["CrossSection"] = CrossSection
    AiRefErr["CrossSection"] = CrossSection_err
    return AiRef, AiRefErr

def CalcArefAndUns(files_path = ""):
    #files_path = "/eos/user/g/gtolkach/WAi/pdf_output/rev/CC_WMenu_WAi_pt_WMenu_v1.0/mainScanTruth/"
    file_list = os.listdir(files_path)
    root_files_dict = {}
    pdf_scale = 1
    pdf_name = "CT"

    unsum_list = ["CT", "MSHT"]
    
    scale_pdf_flag = 0
    for file in file_list:
        if "." in file: continue
        root_files_dict[file] = ROOT.TFile(files_path+file+ "/hists.root")
        if "Scale" in file:
            scale_pdf_flag+=1
    #Nominal
    hist = {}
    hists_wi = {}
    hists_wiwi = {}
    for file in root_files_dict:
        hist[file] = root_files_dict[file].Get("ptll_mll_yll")
        hists_wi[file] = []
        hists_wiwi[file] = []
        for Ai in range(Ai_n):
            hists_wi[file].append(root_files_dict[file].Get("ptll_mll_yll_w%s"%Ai))
            hists_wiwi[file].append(root_files_dict[file].Get("ptll_mll_yll_w%sw%s"%(Ai,Ai)))
    
    AiRef, AiRefErr = GetAref(hist["Nominal"], hists_wi["Nominal"], hists_wiwi["Nominal"])
    # pdf uns GET ONLY MEAN VALUE
    # PDF1Hi/   PDF23Lo/
    n_pdf = 0
    if "NNPDF" in files_path:
        n_pdf = len(root_files_dict) - 1 - 2
        print("NNPDF n_PDF", n_pdf)
        scale_pdf_flag = 1
        
    if any(pdf_ in files_path for pdf_ in unsum_list):
        n_pdf = int((len(root_files_dict) - 1-scale_pdf_flag)/2)
        print("n_PDF", n_pdf)
        
    pdf_err_hi = {}
    pdf_err_lo = {}
    if n_pdf != 0:
        for i in range(1,n_pdf+1):
            name_pdf_Hi = "PDF%sHi"%i
            name_pdf_Lo = "PDF%sLo"%i
            if "NNPDF" in files_path:
                name_pdf_Lo = name_pdf_Hi
            
            AiRef_pdf_Hi, _ = GetAref(hist[name_pdf_Hi], hists_wi[name_pdf_Hi], hists_wiwi[name_pdf_Hi])
            AiRef_pdf_Lo = 0
            if "NNPDF" in files_path:
                AiRef_pdf_Lo = AiRef_pdf_Hi
            else:
                AiRef_pdf_Lo, _ = GetAref(hist[name_pdf_Lo], hists_wi[name_pdf_Lo], hists_wiwi[name_pdf_Lo])
            for Ai in AiRef:
                if Ai not in pdf_err_hi:
                    pdf_err_hi[Ai] = {}
                    pdf_err_lo[Ai] = {}
                for ibin in range(len(AiRef[Ai])):
                    delta_pdf_hi = 0
                    delta_pdf_lo = 0
                    if any(pdf_ in files_path for pdf_ in unsum_list):
                        delta_pdf_hi = max([0, AiRef_pdf_Hi[Ai][ibin]-AiRef[Ai][ibin],AiRef_pdf_Lo[Ai][ibin]-AiRef[Ai][ibin]])
                        delta_pdf_lo = max([0, AiRef[Ai][ibin]-AiRef_pdf_Hi[Ai][ibin], AiRef[Ai][ibin]- AiRef_pdf_Lo[Ai][ibin]])
                    elif "NNPDF" in files_path:
                        delta_pdf_hi = AiRef[Ai][ibin] - AiRef_pdf_Hi[Ai][ibin]
                        delta_pdf_lo = AiRef[Ai][ibin] - AiRef_pdf_Lo[Ai][ibin]
                    if ibin not in pdf_err_hi[Ai]:
                        pdf_err_hi[Ai][ibin] = delta_pdf_hi**2
                        pdf_err_lo[Ai][ibin] = delta_pdf_lo**2

                    else:
                        pdf_err_hi[Ai][ibin] += delta_pdf_hi**2
                        pdf_err_lo[Ai][ibin] += delta_pdf_lo**2        
                    if i == n_pdf:
                        pdf_err_hi[Ai][ibin] =  (pdf_err_hi[Ai][ibin]**0.5)
                        pdf_err_lo[Ai][ibin] =  (pdf_err_lo[Ai][ibin]**0.5)


    pdf_scale_err_hi = {}
    pdf_scale_err_lo = {}

    if scale_pdf_flag != 0:
        if any(pdf_ in files_path for pdf_ in unsum_list):
            name_scale_Hi = "PDFScaleHi"
            name_scale_Lo = "PDFScaleLo"
            print(files_path)
            AiRef_scale_Hi, _ = GetAref(hist[name_scale_Hi], hists_wi[name_scale_Hi], hists_wiwi[name_scale_Hi])
            AiRef_scale_Lo, _ = GetAref(hist[name_scale_Lo], hists_wi[name_scale_Lo], hists_wiwi[name_scale_Lo])
        elif "NNPDF" in files_path:
            name_scale_Hi = "PDF%sHi"%(n_pdf+2)
            name_scale_Lo = "PDF%sHi"%(n_pdf+1)
            AiRef_scale_Hi, _ = GetAref(hist[name_scale_Hi], hists_wi[name_scale_Hi], hists_wiwi[name_scale_Hi])
            AiRef_scale_Lo, _ = GetAref(hist[name_scale_Lo], hists_wi[name_scale_Lo], hists_wiwi[name_scale_Lo])
        else: 
            print("ERROR")
            exit(1)
        #print(AiRef_scale_Hi)
        for Ai in AiRef:
            if Ai not in pdf_scale_err_hi:
                pdf_scale_err_hi[Ai] = {}
                pdf_scale_err_lo[Ai] = {}
            for ibin in range(len(AiRef[Ai])):
                delta_scale_hi = 0
                delta_scale_lo = 0
                if any(pdf_ in files_path for pdf_ in unsum_list):
                    delta_scale_hi = max([0, AiRef_scale_Hi[Ai][ibin]-AiRef[Ai][ibin],AiRef_scale_Lo[Ai][ibin]-AiRef[Ai][ibin]])
                    delta_scale_lo = max([0, AiRef[Ai][ibin]-AiRef_scale_Hi[Ai][ibin], AiRef[Ai][ibin]- AiRef_scale_Lo[Ai][ibin]])
                elif "NNPDF" in files_path:
                    delta_scale_hi = (AiRef_scale_Hi[Ai][ibin] -  AiRef_scale_Lo[Ai][ibin])/2
                    delta_scale_lo = delta_scale_hi
                    
                pdf_scale_err_hi[Ai][ibin] = delta_scale_hi
                pdf_scale_err_lo[Ai][ibin] = delta_scale_lo

    return AiRef, AiRefErr, pdf_err_hi, pdf_err_lo, pdf_scale_err_hi, pdf_scale_err_lo
    
def PrintArefAndUnc(AiRef, AiRefErr, pdf_err_hi, pdf_err_lo, pdf_scale_err_hi, pdf_scale_err_lo ):
    for Ai in AiRef:
        for ibin in range(len(AiRef[Ai])):
            if len(pdf_err_hi)!=0:
                print(Ai,"bin:",ibin, round(AiRef[Ai][ibin],5),
                    "+-",round(AiRefErr[Ai][ibin],5),"(stat)",
                    "+",round(pdf_err_hi[Ai][ibin],5),"-",round(pdf_err_lo[Ai][ibin],5),"(pdf)",
                    "+",round(pdf_scale_err_hi[Ai][ibin],5),"-",round(pdf_scale_err_lo[Ai][ibin],5),"(scale)")
            else:
                print(Ai,"bin:",ibin, round(AiRef[Ai][ibin],5),
                    "+-",round(AiRefErr[Ai][ibin],5), "(stat)")

def CalcAiDiffHist(AiRef_A, AiRefErr_A, pdf_err_hi_A, pdf_err_lo_A,pdf_scale_err_hi_A, pdf_scale_err_lo_A,
                   AiRef_B, AiRefErr_B, pdf_err_hi_B, pdf_err_lo_B,pdf_scale_err_hi_B, pdf_scale_err_lo_B, var = "pt",num=0):
    if "pt" in var:
        binlist = [0, 8., 17., 27., 40., 55., 75., 110., 150., 210., 600.]
    elif "y" in var:
        binlist = [0, 0.4, 0.8, 1.2, 1.6, 2.0, 2.4, 3.6]
    bining = array("d" , binlist )
    n_BINS = len(bining)-1
    hist_Ai = {}
    hist_Ai_graph = {}
    
    hist_Ai_stat = {}
    hist_Ai_stat_graph = {}
    
    hist_Ai_pdf = {}
    hist_Ai_pdf_graph = {}
    hist_Ai_scale = {}
    hist_Ai_scale_graph = {}
    hist_Ai_total = {}
    hist_Ai_total_graph = {}
    if len(AiRef_A) !=len(AiRef_B):
        print("ERROR: (AiRef_A) !=len(AiRef_B)")
        exit(123)
    for Ai in AiRef_A:        
        hist_Ai[Ai] = ROOT.TH1D("h%s"%(Ai),"; ; A_{%s}"%(Ai), n_BINS,bining)
        hist_Ai_graph[Ai] = ROOT.TGraphAsymmErrors( hist_Ai[Ai] )
        hist_Ai_stat[Ai] = ROOT.TH1D("hstat%s"%(Ai),"; ; A_{%s}"%(Ai), n_BINS,bining)
        hist_Ai_stat_graph[Ai] = ROOT.TGraphAsymmErrors( hist_Ai[Ai] )
        hist_Ai_pdf[Ai] = ROOT.TH1D("hpdf%s"%(Ai),"; ; A_{%s}"%(Ai), n_BINS,bining)
        hist_Ai_pdf_graph[Ai] = ROOT.TGraphAsymmErrors( hist_Ai[Ai] )
        hist_Ai_scale[Ai] = ROOT.TH1D("hscale%s"%(Ai),"; ; A_{%s}"%(Ai), n_BINS,bining)
        hist_Ai_scale_graph[Ai] = ROOT.TGraphAsymmErrors( hist_Ai[Ai] )
        hist_Ai_total[Ai] = ROOT.TH1D("htotal%s"%(Ai),"; ; A_{%s}"%(Ai), n_BINS,bining)
        hist_Ai_total_graph[Ai] = ROOT.TGraphAsymmErrors( hist_Ai[Ai] )


        
    for Ai in AiRef_A:
        for ibin in range(len(AiRef_A[Ai])):
            #if Ai == "A4": print(ibin, AiRef[Ai][ibin],sqrt(AiRefErr[Ai][ibin]**2 + pdf_err_lo[Ai][ibin]**2))
            pdf_lo_A = 0 
            pdf_hi_A = 0
            scale_hi_A = 0
            scale_lo_A = 0
            stat_A = 0
            
            pdf_lo_B = 0 
            pdf_hi_B = 0
            scale_hi_B = 0
            scale_lo_B = 0
            stat_B = 0
            
            if len(pdf_err_hi_A)!=0:
                pdf_lo_A = pdf_err_lo_A[Ai][ibin]
                pdf_hi_A = pdf_err_hi_A[Ai][ibin]
                
            if len(pdf_err_hi_B)!=0:
                print("point1 ",Ai)
                pdf_lo_B = pdf_err_lo_B[Ai][ibin]
                pdf_hi_B = pdf_err_hi_B[Ai][ibin]
                
            if len(pdf_scale_err_hi_A)!=0:
                scale_hi_A = pdf_scale_err_hi_A[Ai][ibin]
                scale_lo_A = pdf_scale_err_lo_A[Ai][ibin]
                
            if len(pdf_scale_err_hi_B)!=0:
                scale_hi_B = pdf_scale_err_hi_B[Ai][ibin]
                scale_lo_B = pdf_scale_err_lo_B[Ai][ibin]
                
            if len(AiRefErr_A) != 0:
                stat_A = AiRefErr_A[Ai][ibin]
            if len(AiRefErr_B) != 0:
                stat_B = AiRefErr_B[Ai][ibin]
            
            if "CrossSection" not in Ai:
                hist_Ai_graph[Ai].SetPointY(ibin, AiRef_A[Ai][ibin]-AiRef_B[Ai][ibin])
                hist_Ai_graph[Ai].SetPointEYlow(ibin, sqrt(stat_A**2 + pdf_lo_A**2 + scale_lo_A**2+stat_B**2 + pdf_lo_B**2 + scale_lo_B**2))
                hist_Ai_graph[Ai].SetPointEYhigh(ibin,sqrt(stat_A**2 + pdf_hi_A**2 + scale_hi_A**2+stat_B**2 + pdf_hi_B**2 + scale_hi_B**2))
                total_stat_2 = stat_A**2+stat_B**2
                total_pdf_2 = ((pdf_lo_A+pdf_hi_A)/2)**2+((pdf_lo_B+pdf_hi_B)/2)**2
                total_scale_2 = ((scale_lo_A+scale_hi_A)/2)**2 +((scale_lo_B+scale_hi_B)/2)**2 
                hist_Ai_stat_graph[Ai].SetPointY(ibin, sqrt(total_stat_2))
                hist_Ai_pdf_graph[Ai].SetPointY(ibin, sqrt(total_pdf_2))
                hist_Ai_scale_graph[Ai].SetPointY(ibin,sqrt(total_scale_2))
                hist_Ai_total_graph[Ai].SetPointY(ibin,sqrt(total_stat_2 + total_pdf_2+ total_scale_2))
            else:
                hist_Ai_graph[Ai].SetPointY(ibin, AiRef_B[Ai][ibin]/AiRef_A[Ai][ibin])
                tot_err_A_lo = sqrt(stat_A**2 + pdf_lo_A**2 + scale_lo_A**2)
                tot_err_B_lo = sqrt(stat_B**2 + pdf_lo_B**2 + scale_lo_B**2)
                tot_err_A_hi = sqrt(stat_A**2 + pdf_hi_A**2 + scale_hi_A**2)
                tot_err_B_hi = sqrt(stat_B**2 + pdf_hi_B**2 + scale_hi_B**2)
                tot_lo = (tot_err_A_lo*AiRef_B[Ai][ibin]+ tot_err_B_lo*AiRef_A[Ai][ibin])/(AiRef_A[Ai][ibin])**2
                tot_hi = (tot_err_A_hi*AiRef_B[Ai][ibin]+ tot_err_B_hi*AiRef_A[Ai][ibin])/(AiRef_A[Ai][ibin])**2
                hist_Ai_graph[Ai].SetPointEYlow(ibin, tot_lo)
                hist_Ai_graph[Ai].SetPointEYhigh(ibin, tot_hi)
                          
            hist_Ai_graph[Ai].SetPoint(ibin, hist_Ai_graph[Ai].GetPointX(ibin) - 0.01*num, hist_Ai_graph[Ai].GetPointY(ibin)-0.01*num)

                
                                              
    return hist_Ai_graph.copy(), hist_Ai_stat_graph.copy(), hist_Ai_pdf_graph.copy(), hist_Ai_scale_graph.copy(), hist_Ai_total_graph.copy()
    

def CalcAiDiffHist_v1(AiRef_A, AiRefErr_A, pdf_err_hi_A, pdf_err_lo_A,pdf_scale_err_hi_A, pdf_scale_err_lo_A,
                   AiRef_B, AiRefErr_B, pdf_err_hi_B, pdf_err_lo_B,pdf_scale_err_hi_B, pdf_scale_err_lo_B, var = "pt",num=0):
    if "pt" in var:
        binlist = [0, 8., 17., 27., 40., 55., 75., 110., 150., 210., 600.]
    elif "y" in var:
        binlist = [0, 0.4, 0.8, 1.2, 1.6, 2.0, 2.4, 3.6]
    bining = array("d" , binlist )
    n_BINS = len(bining)-1
    hist_Ai = {}
    hist_Ai_graph = {}
    
    hist_Ai_stat = {}
    hist_Ai_stat_graph = {}
    
    hist_Ai_pdf = {}
    hist_Ai_pdf_graph = {}
    hist_Ai_scale = {}
    hist_Ai_scale_graph = {}
    hist_Ai_total = {}
    hist_Ai_total_graph = {}
    for Ai in AiRef_A:
        points = []
        cen = []
        p1s = []
        n1s = []
        exl = []
        exh = []
        for ibin in range(len(AiRef_A[Ai])):
            pdf_lo_A = 0 
            pdf_hi_A = 0
            scale_hi_A = 0
            scale_lo_A = 0
            stat_A = 0
            
            pdf_lo_B = 0 
            pdf_hi_B = 0
            scale_hi_B = 0
            scale_lo_B = 0
            stat_B = 0
            
            if len(pdf_err_hi_A)!=0:
                pdf_lo_A = pdf_err_lo_A[Ai][ibin]
                pdf_hi_A = pdf_err_hi_A[Ai][ibin]
                
            if len(pdf_err_hi_B)!=0:
                pdf_lo_B = pdf_err_lo_B[Ai][ibin]
                pdf_hi_B = pdf_err_hi_B[Ai][ibin]
                
            if len(pdf_scale_err_hi_A)!=0:
                scale_hi_A = pdf_scale_err_hi_A[Ai][ibin]
                scale_lo_A = pdf_scale_err_lo_A[Ai][ibin]
                
            if len(pdf_scale_err_hi_B)!=0:
                scale_hi_B = pdf_scale_err_hi_B[Ai][ibin]
                scale_lo_B = pdf_scale_err_lo_B[Ai][ibin]
                
            if len(AiRefErr_A) != 0:
                stat_A = AiRefErr_A[Ai][ibin]
            if len(AiRefErr_B) != 0:
                stat_B = AiRefErr_B[Ai][ibin]
            
            if "CrossSection" in Ai: 
                tot_err_A_lo = sqrt(stat_A**2 + pdf_lo_A**2 + scale_lo_A**2)
                tot_err_B_lo = sqrt(stat_B**2 + pdf_lo_B**2 + scale_lo_B**2)
                tot_err_A_hi = sqrt(stat_A**2 + pdf_hi_A**2 + scale_hi_A**2)
                tot_err_B_hi = sqrt(stat_B**2 + pdf_hi_B**2 + scale_hi_B**2)

                tot_lo = (tot_err_A_lo*AiRef_B[Ai][ibin]+ tot_err_B_lo*AiRef_A[Ai][ibin])/(AiRef_A[Ai][ibin])**2
                tot_hi = (tot_err_A_hi*AiRef_B[Ai][ibin]+ tot_err_B_hi*AiRef_A[Ai][ibin])/(AiRef_A[Ai][ibin])**2
                cen.append(AiRef_B[Ai][ibin]/AiRef_A[Ai][ibin])
                p1s.append(tot_hi)
                n1s.append(tot_lo)
            else:
                tot_lo = sqrt(stat_A**2 + pdf_lo_A**2 + scale_lo_A**2+stat_B**2 + pdf_lo_B**2 + scale_lo_B**2)
                tot_hi = sqrt(stat_A**2 + pdf_hi_A**2 + scale_hi_A**2+stat_B**2 + pdf_hi_B**2 + scale_hi_B**2)
                cen.append(AiRef_A[Ai][ibin]-AiRef_B[Ai][ibin])
                p1s.append(tot_hi)
                n1s.append(tot_lo)
                
                
            offset = -0.05*(binlist[ibin] - binlist[ibin+1])*(num)
            points.append(0.5*(binlist[ibin] + binlist[ibin+1]) + offset)
            exl.append(0.5*(binlist[ibin] + binlist[ibin+1]) - binlist[ibin] + offset)
            exh.append(binlist[ibin+1] - 0.5*(binlist[ibin] + binlist[ibin+1]) - offset)
            
        points = array("d" , points )
        cen = array("d" , cen )
        exl = array("d" , exl )
        exh = array("d" , exh )
        n1s = array("d" , n1s )
        p1s = array("d" , p1s )
        hist_Ai_graph[Ai] = ROOT.TGraphAsymmErrors(n_BINS, points, cen, exl, exh, n1s, p1s)
    return hist_Ai_graph.copy(), hist_Ai_stat_graph.copy(), hist_Ai_pdf_graph.copy(), hist_Ai_scale_graph.copy(), hist_Ai_total_graph.copy()

def fillAiHist( AiRef, AiRefErr, pdf_err_hi, pdf_err_lo,pdf_scale_err_hi, pdf_scale_err_lo, var = "pt"):
    if "pt0" in var:
        binlist = [0, 8., 17., 27., 40., 55., 75., 110., 150., 210., 600.]
    elif "y" in var:
        binlist = [0, 0.4, 0.8, 1.2, 1.6, 2.0, 2.4, 3.6]
    elif "pt2" in var:
        binlist = [0.0, 5.0, 10.0, 15.0, 20.0, 25.0, 30.0, 35.0, 40.0, 45.0, 50.0, 55.0, 60.0, 65.0, 70.0,
                 75.0, 80.0, 85.0, 90.0, 95.0, 100.0, 105.0, 110.0, 115.0, 120.0, 125.0, 130.0, 135.0]

    bining = array("d" , binlist )
    n_BINS = len(bining)-1
    hist_Ai = {}
    hist_Ai_graph = {}
    
    hist_Ai_stat = {}
    hist_Ai_stat_graph = {}
    
    hist_Ai_pdf = {}
    hist_Ai_pdf_graph = {}
    hist_Ai_scale = {}
    hist_Ai_scale_graph = {}
    hist_Ai_total = {}
    hist_Ai_total_graph = {}
    

    hist_Ai_total_pdf_graph = {}
    
    for Ai in AiRef:        
        hist_Ai[Ai] = ROOT.TH1D("h%s"%(Ai),"; ; A_{%s}"%(Ai), n_BINS,bining)
        hist_Ai_graph[Ai] = ROOT.TGraphAsymmErrors( hist_Ai[Ai] )
        
        hist_Ai_stat[Ai] = ROOT.TH1D("hstat%s"%(Ai),"; ; A_{%s}"%(Ai), n_BINS,bining)
        hist_Ai_stat_graph[Ai] = ROOT.TGraphAsymmErrors( hist_Ai[Ai] )
        hist_Ai_pdf[Ai] = ROOT.TH1D("hpdf%s"%(Ai),"; ; A_{%s}"%(Ai), n_BINS,bining)
        hist_Ai_pdf_graph[Ai] = ROOT.TGraphAsymmErrors( hist_Ai[Ai] )
        hist_Ai_scale[Ai] = ROOT.TH1D("hscale%s"%(Ai),"; ; A_{%s}"%(Ai), n_BINS,bining)
        hist_Ai_scale_graph[Ai] = ROOT.TGraphAsymmErrors( hist_Ai[Ai] )
        hist_Ai_total[Ai] = ROOT.TH1D("htotal%s"%(Ai),"; ; A_{%s}"%(Ai), n_BINS,bining)
        hist_Ai_total_graph[Ai] = ROOT.TGraphAsymmErrors( hist_Ai[Ai] )
        hist_Ai_total_pdf_graph[Ai] = ROOT.TGraphAsymmErrors( hist_Ai[Ai] )

    for Ai in AiRef:
        for ibin in range(len(AiRef[Ai])):
            #if Ai == "A4": print(ibin, AiRef[Ai][ibin],sqrt(AiRefErr[Ai][ibin]**2 + pdf_err_lo[Ai][ibin]**2))
            pdf_lo = 0 
            pdf_hi = 0
            scale_hi = 0
            scale_lo = 0
            stat = 0
            if len(pdf_err_hi)!=0:
                pdf_lo = pdf_err_lo[Ai][ibin]
                pdf_hi = pdf_err_hi[Ai][ibin]
                
            if len(pdf_scale_err_hi)!=0:
                scale_hi = pdf_scale_err_hi[Ai][ibin]
                scale_lo = pdf_scale_err_lo[Ai][ibin]
            if len(AiRefErr) != 0:
                stat = AiRefErr[Ai][ibin]
            hist_Ai_graph[Ai].SetPointY(ibin, AiRef[Ai][ibin])
            hist_Ai_graph[Ai].SetPointEYlow(ibin, sqrt(stat**2 + pdf_lo**2 + scale_lo**2))
            hist_Ai_graph[Ai].SetPointEYhigh(ibin,sqrt(stat**2 + pdf_hi**2 + scale_hi**2))
            
            
            #if "4" in Ai: print(ibin, stat)
            
            hist_Ai_stat_graph[Ai].SetPointY(ibin, stat)
            pdf_unc = max([abs(pdf_lo), abs(pdf_hi)])
            pdf_scale_unc = max([abs(scale_lo), abs(scale_hi)])


            hist_Ai_pdf_graph[Ai].SetPointY(ibin, pdf_unc)
            hist_Ai_scale_graph[Ai].SetPointY(ibin, pdf_scale_unc)
            hist_Ai_total_graph[Ai].SetPointY(ibin,sqrt(stat**2 + pdf_unc**2 + pdf_scale_unc**2))
            hist_Ai_total_pdf_graph[Ai].SetPointY(ibin, sqrt(pdf_unc**2 + pdf_scale_unc**2))


    return hist_Ai_graph.copy(), hist_Ai_stat_graph.copy(), hist_Ai_pdf_graph.copy(), hist_Ai_scale_graph.copy(), hist_Ai_total_graph.copy(), hist_Ai_total_pdf_graph.copy()
        
def readArefAndUnsFromFile(Ai,file_name, var):
    if "pt" in var:
        binlist = [0, 8., 17., 27., 40., 55., 75., 110., 150., 210., 600.]
    elif "y" in var:
        binlist = [0, 0.4, 0.8, 1.2, 1.6, 2.0, 2.4, 3.6]
    bining = array("d" , binlist )
    n_BINS = len(bining)-1
    
    hist_Ai = ROOT.TH1D("h%s"%(Ai),"; ; A_{%s}"%(Ai), n_BINS,bining)
    hist_Ai_graph = ROOT.TGraphAsymmErrors( hist_Ai )

    f_txt = open(file_name)
    list_from_file = f_txt.read().split("\n")
    #mean_value = {}
    #pdf_hi = {}
    #pdf_lo = {}
    for i in range(len(list_from_file)):
        list_2 = list_from_file[i].split(" ")
        if len(list_2)<2: continue
        delta = 1.0
        if Ai == 9:
            delta = binlist[i+1] - binlist[i]
        #mean_value[list_2[0]] = list_2[3*Ai+1]
        #pdf_hi[list_2[0]] = list_2[3*Ai+2]
        #pdf_lo[list_2[0]] = list_2[3*Ai+3]
        print(delta)
        print(i, float(list_2[3*Ai+1])/delta,float(list_2[3*Ai+2])/delta, float(list_2[3*Ai+3])/delta)
        #hist_Ai_graph.SetPointY(i, float(list_2[3*Ai+2]))
        #hist_Ai_graph.SetPointEYlow(i,0)
        #hist_Ai_graph.SetPointEYhigh(i,0)
        hist_Ai_graph.SetPointY(i, 0)
        hist_Ai_graph.SetPointEYlow(i,float(list_2[3*Ai+3])/delta)
        hist_Ai_graph.SetPointEYhigh(i,float(list_2[3*Ai+2])/delta)
    
    
    return hist_Ai_graph



def fillAiHist_v1( AiRef, AiRefErr, pdf_err_hi, pdf_err_lo,pdf_scale_err_hi, pdf_scale_err_lo, var = "pt"):
    if "pt" in var:
        binlist = [0, 8., 17., 27., 40., 55., 75., 110., 150., 210., 600.]
    elif "y" in var:
        binlist = [0, 0.4, 0.8, 1.2, 1.6, 2.0, 2.4, 3.6]
    bining = array("d" , binlist )
    n_BINS = len(bining)-1
    hist_Ai = {}
    hist_Ai_graph = {}
    
    hist_Ai_stat = {}
    hist_Ai_stat_graph = {}
    
    hist_Ai_pdf = {}
    hist_Ai_pdf_graph = {}
    hist_Ai_scale = {}
    hist_Ai_scale_graph = {}
    hist_Ai_total = {}
    hist_Ai_total_graph = {}
    

    hist_Ai_total_pdf_graph = {}
    
    for Ai in AiRef:        
        hist_Ai[Ai] = ROOT.TH1D("h%s"%(Ai),"; ; A_{%s}"%(Ai), n_BINS,bining)
        hist_Ai_graph[Ai] = ROOT.TGraphAsymmErrors( hist_Ai[Ai] )
        
        hist_Ai_stat[Ai] = ROOT.TH1D("hstat%s"%(Ai),"; ; A_{%s}"%(Ai), n_BINS,bining)
        hist_Ai_stat_graph[Ai] = ROOT.TGraphAsymmErrors( hist_Ai[Ai] )
        hist_Ai_pdf[Ai] = ROOT.TH1D("hpdf%s"%(Ai),"; ; A_{%s}"%(Ai), n_BINS,bining)
        hist_Ai_pdf_graph[Ai] = ROOT.TGraphAsymmErrors( hist_Ai[Ai] )
        hist_Ai_scale[Ai] = ROOT.TH1D("hscale%s"%(Ai),"; ; A_{%s}"%(Ai), n_BINS,bining)
        hist_Ai_scale_graph[Ai] = ROOT.TGraphAsymmErrors( hist_Ai[Ai] )
        hist_Ai_total[Ai] = ROOT.TH1D("htotal%s"%(Ai),"; ; A_{%s}"%(Ai), n_BINS,bining)
        hist_Ai_total_graph[Ai] = ROOT.TGraphAsymmErrors( hist_Ai[Ai] )
        hist_Ai_total_pdf_graph[Ai] = ROOT.TGraphAsymmErrors( hist_Ai[Ai] )

    for Ai in AiRef:
        for ibin in range(len(AiRef[Ai])):
            #if Ai == "A4": print(ibin, AiRef[Ai][ibin],sqrt(AiRefErr[Ai][ibin]**2 + pdf_err_lo[Ai][ibin]**2))
            pdf_lo = 0 
            pdf_hi = 0
            scale_hi = 0
            scale_lo = 0
            stat = 0
            if len(pdf_err_hi)!=0:
                pdf_lo = pdf_err_lo[Ai][ibin]
                pdf_hi = pdf_err_hi[Ai][ibin]
                
            if len(pdf_scale_err_hi)!=0:
                scale_hi = pdf_scale_err_hi[Ai][ibin]
                scale_lo = pdf_scale_err_lo[Ai][ibin]
            if len(AiRefErr) != 0:
                stat = AiRefErr[Ai][ibin]
            hist_Ai_graph[Ai].SetPointY(ibin, AiRef[Ai][ibin])
            hist_Ai_graph[Ai].SetPointEYlow(ibin, sqrt(stat**2 + pdf_lo**2 + scale_lo**2))
            hist_Ai_graph[Ai].SetPointEYhigh(ibin,sqrt(stat**2 + pdf_hi**2 + scale_hi**2))
            
            
            #if "4" in Ai: print(ibin, stat)
            
            hist_Ai_stat_graph[Ai].SetPointY(ibin, stat)
            pdf_unc = max([abs(pdf_lo), abs(pdf_hi)])
            pdf_scale_unc = max([abs(scale_lo), abs(scale_hi)])


            hist_Ai_total_pdf_graph[Ai].SetPointY(ibin, 0)
            hist_Ai_total_pdf_graph[Ai].SetPointEYlow(ibin, sqrt(pdf_lo**2+scale_lo**2))
            hist_Ai_total_pdf_graph[Ai].SetPointEYhigh(ibin,sqrt(pdf_hi**2+scale_hi**2))

            hist_Ai_scale_graph[Ai].SetPointY(ibin, pdf_scale_unc)
            hist_Ai_total_graph[Ai].SetPointY(ibin,sqrt(stat**2 + pdf_unc**2 + pdf_scale_unc**2))
            #hist_Ai_total_pdf_graph[Ai].SetPointY(ibin, sqrt(pdf_unc**2 + pdf_scale_unc**2))


    return hist_Ai_graph.copy(), hist_Ai_stat_graph.copy(), hist_Ai_pdf_graph.copy(), hist_Ai_scale_graph.copy(), hist_Ai_total_graph.copy(), hist_Ai_total_pdf_graph.copy()



def readAiFromDYTURBOfiles(Ai,file_name, var):
    if "pt" in var:
        binlist = [0, 8., 17., 27., 40., 55., 75., 110., 150., 210., 600.]
    elif "y" in var:
        binlist = [0, 0.4, 0.8, 1.2, 1.6, 2.0, 2.4, 3.6]    
    hist_Ai = ROOT.TH1D("h%s"%(Ai),"; ; A_{%s}"%(Ai), 27, 0 , 135)
    #hist_Ai_graph = ROOT.TGraphAsymmErrors( hist_Ai )

    f_txt = open(file_name)
    list_from_file = f_txt.read().split("\n")
    mean_value = {}
    pdf_hi = {}
    pdf_lo = {}
    for i in range(len(list_from_file)):
        list_2 = list_from_file[i].split(" ")
        if type(list_2) is list:
            value = list_2[2]
            err = list_2[3]
            hist_Ai.SetBinContent(i+1,float(value))
            print(i, value, err)
    return hist_Ai