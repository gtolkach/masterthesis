#DrawAndSaveAiUns
import ROOT
from filereader.filereader import CalcArefAndUns, PrintArefAndUnc, fillAiHist
def DrawAndSaveAi(hist_Ai, Ai, save_dir, pdf_name,var, channel,version, logYex=False):
    hist_Ai = hist_Ai.Clone("Clone")
    legend = ROOT.TLegend(0.15, 0.65,0.35,0.85,"", "NDC")
    frac_insert = 0.3 + 0.94 - 0.87
    header = ""
    if "WMenu" in channel:
        header = "W^{-} #rightarrow e^{-} #nu"
    elif "WPenu" in channel:
        header = "W^{+} #rightarrow e^{+} #nu"
        
    legend.SetHeader(header,"C")

    legend.SetFillStyle(0)
    legend.SetTextSize(0.05)
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetTextFont(42)
    #leg = ROOT.TLegend(xmin_leg, ymax_leg-ydiff_leg, xmin_leg+xdiff_leg, ymax_leg, )
    c = ROOT.TCanvas("c1","c1",1024,768)
    if logYex:
        ROOT.gPad.SetLogx()

    mg = ROOT.TMultiGraph();
    ROOT.gPad.SetTickx()
    ROOT.gPad.SetTicky()
    ROOT.gStyle.SetEndErrorSize(7)

    hist_Ai.SetTitle("")
    hist_Ai.SetMarkerColor(1)
    hist_Ai.SetLineColor(1)
    hist_Ai.SetMarkerStyle(8)
    hist_Ai.SetMarkerSize(1.5)
    hist_Ai.SetLineWidth(3)



#    hist_Ai.GetYaxis().SetTitleOffset(1.2)
    hist_Ai.GetYaxis().SetMoreLogLabels(1)
    hist_Ai.GetYaxis().SetNoExponent(1)
    #hist_Ai.GetYaxis().SetRangeUser(-0.2,1.6)
    #hist_Ai.GetXaxis().SetRangeUser(-10,600+10)

    #hist_Ai.GetXaxis().SetTitle("p_{T}^{l#nu} [GeV]")
    #hist_Ai.GetYaxis().SetTitle("A_{i}")

    mg.Add(hist_Ai, "pz")
    legend.AddEntry(hist_Ai, pdf_name,"pl")




    gr1 = ROOT.TGraph(2)
    gr1.SetPoint(0, -1, 0)
    gr1.SetPoint(1, 1000, 0)
    gr1.SetLineStyle(7)
    ROOT.gPad.RedrawAxis()

    mg.Draw("a")
    mg.GetYaxis().SetTitleOffset(1.4)
    if var == "pt":
        mg.GetXaxis().SetTitle("p_{T}^{l#nu} [GeV]")
        mg.GetXaxis().SetRangeUser(-10,610)
        if Ai in ["0","2","3","4"]: mg.GetYaxis().SetRangeUser(-0.2, 1.6)
        elif Ai in "1": mg.GetYaxis().SetRangeUser(-0.5, 0.5)
        elif Ai in ["5","6","7","4"]:mg.GetYaxis().SetRangeUser(-0.1, 0.1)
        elif Ai in "CS":mg.GetYaxis().SetRangeUser(0,mg.GetYaxis().GetXmax()*3/2)
    
    elif var == "y":
        mg.GetXaxis().SetTitle("|y^{l#nu}|")
        mg.GetXaxis().SetRangeUser(-0.2,3.8)
        ymax = mg.GetYaxis().GetXmax();
        ymin = min(0.0, mg.GetYaxis().GetXmin())
        dy0 = ymax - ymin
        dy1 = frac_insert / (1 - frac_insert) * dy0
        dy_tot = dy1 + dy0
        mg.GetYaxis().SetRangeUser(ymin, ymin + dy_tot)

    if Ai not in "CS":    
        mg.GetYaxis().SetTitle("A_{%s}"%Ai)
    else:
        mg.GetYaxis().SetTitle("d^{3}#sigma / dp^{W}_{T}dm^{W}dy^{y}")




    
    

    #hist_Ai_graph["A4"].Draw("ap")
    gr1.Draw("same")
    legend.Draw("same")
    c.Draw()
    save_name = "predict_"+pdf_name+"_"+var+"_"+"_A"+Ai
    c.SaveAs(save_dir+"_"+version+"/"+save_name+".pdf")
    c.SaveAs(save_dir+"_"+version+"/"+save_name+".png")

def main():
    Ai_n = 8
    
    #pt

    files_path = "/afs/cern.ch/work/g/gtolkach/application/masterthesis/ConvertScripts/output/lowest_order_wpluesenu_MG/"
    
    #y
    output_dir = "plots/lowest_order_mg5_wpluesenu"
    channel =  "WPenu"
    pdf_name = "MG5_lowest_order"
    var = "pt"
    ver = "v1"
    log = True
    AiRef, AiRefErr, pdf_err_hi, pdf_err_lo,  pdf_scale_err_hi, pdf_scale_err_lo  = CalcArefAndUns(files_path)
    #hist_Ai_graph = fillAiHist(AiRef, AiRefErr, pdf_err_hi, pdf_err_lo, pdf_scale_err_hi, pdf_scale_err_lo , "pt")
    hist_Ai_graph, hist_Ai_stat_graph, hist_Ai_pdf_graph, hist_Ai_scale_graph, hist_Ai_total_graph, _ = fillAiHist(AiRef, AiRefErr,
                                                                                                                pdf_err_hi, pdf_err_lo,
                                                                                                                pdf_scale_err_hi, pdf_scale_err_lo,var)

    for Ai in hist_Ai_graph:
        if "CrossSection" not in Ai:
            DrawAndSaveAi(hist_Ai_graph[Ai], Ai.split("A")[1],output_dir, pdf_name, var, channel, version=ver,logYex=log)
            #DrawAndSaveAiBreakdown(hist_Ai_stat_graph[Ai], hist_Ai_pdf_graph[Ai], hist_Ai_scale_graph[Ai], hist_Ai_total_graph[Ai],
            #                 Ai.split("A")[1], output_dir, pdf_name,var, channel,version=ver, logYex=log)
        else:
            DrawAndSaveAi(hist_Ai_graph[Ai], "CS", output_dir, pdf_name, var, channel,version=ver,logYex=log)
            #DrawAndSaveAiBreakdown(hist_Ai_stat_graph[Ai], hist_Ai_pdf_graph[Ai], hist_Ai_scale_graph[Ai], hist_Ai_total_graph[Ai],
            #                 "CS",  output_dir, pdf_name,var, channel,version=ver, logYex=log)
if __name__ == "__main__":
    main()