def DrawAiPrediction(hist, hist_diff, var, save_dir, channel, Ai,version,logXex=False ):
    #Ai = "7"
    #h_CT10 = hist_Ai_graph_CT10["A7"].Clone("CT10")
    #h_CT18 = hist_Ai_graph_CT18["A7"].Clone("CT18")
    #h_NNPDF = hist_Ai_graph_NNPDF["A7"].Clone("NNPDF")
    #h_CT10_CT18 = graph_CT10_CT18["A7"].Clone("CT10CT18")
    #h_CT10_NNPDF = graph_CT10_NNPDF["A7"].Clone("CT10NNPDF")
    #h_CT10_CT10 = graph_CT10_CT10["A7"].Clone("CT10CT10")
    #var = "pt"
    #channel = "WMenu"
    #logXex = True
    #hist = {"CT10NLO":h_CT10, "CT18NNLO":h_CT18, "NNPDF4.0NNLO": h_NNPDF}
    #hist_diff = {"CT10NLO":h_CT10_CT10, "CT18NNLO":h_CT10_CT18, "NNPDF4.0NNLO":h_CT10_NNPDF}


    c3 =  ROOT.TCanvas("c3", "ph_et in", 0,0,600,600)

    pad_1 = ROOT.TPad("pad1","This is pad1",0.01,0.40,1,1.0)
    pad_2 = ROOT.TPad("pad2","This is pad2",0.01,0.01,1,0.39)
    pad_1.SetBorderSize(0)
    pad_1.SetBottomMargin(0.0)
    pad_1.SetLeftMargin(0.12)

    pad_1.Draw()
    pad_2.SetBorderSize(0)
    pad_2.SetTopMargin(0.)
    pad_2.SetLeftMargin(0.12)
    pad_2.SetRightMargin(1.0)

    pad_2.SetBottomMargin(0.40)
    pad_2.Draw()
    
    color_list = {"CT10":ROOT.kOrange-3 , "CT18NNLO":2,"NNPDF4.0NNLO":ROOT.kAzure+1, "MSHT20NNLO": ROOT.kGreen+3}
    MarketStyle_list = {"CT10":25, "CT18NNLO":32, "NNPDF4.0NNLO": 4,  "MSHT20NNLO":31}
    MarketFill_list = {"CT10":1, "CT18NNLO":1001, "NNPDF4.0NNLO": 3001, "MSHT20NNLO":3345}

    


    
    legend = ROOT.TLegend(0.15, 0.65,0.35,0.85,"", "NDC")
    frac_insert = 0.3 + 0.94 - 0.87
    header = ""
    header = "Powheg+Pythia8,"
    if "WMenu" in channel:
        header += " W^{-} #rightarrow e^{-} #nu"
    elif "WPenu" in channel:
        header += " W^{+} #rightarrow e^{+} #nu"
        print("+")

    legend.SetHeader(header)

    legend.SetFillStyle(0)
    legend.SetTextSize(0.05)
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetTextFont(42)


    mg = ROOT.TMultiGraph()
    mg_2 = ROOT.TMultiGraph()

    pad_1.SetTickx()
    pad_1.SetTicky()
    pad_2.SetTickx()
    pad_2.SetTicky()
    ROOT.gStyle.SetEndErrorSize(7)


    for pdf in hist:
        hist[pdf].SetTitle("")
        hist[pdf].SetMarkerColor(color_list[pdf])
        hist[pdf].SetLineColor(color_list[pdf])
        hist[pdf].SetMarkerStyle(MarketStyle_list[pdf])
        hist[pdf].SetMarkerSize(1.5)
        hist[pdf].SetLineWidth(2)
        #hist[pdf].SetFillStyle(3004)
        #hist[pdf].SetFillColor(color_list[pdf])


        mg.Add(hist[pdf])
        legend.AddEntry(hist[pdf], pdf)

        hist_diff[pdf].SetTitle("")
        hist_diff[pdf].SetMarkerColor(color_list[pdf])
        hist_diff[pdf].SetLineColor(color_list[pdf])
        hist_diff[pdf].SetMarkerStyle(MarketStyle_list[pdf])
        hist_diff[pdf].SetMarkerSize(1.5)
        if "CT18" in pdf:
            hist_diff[pdf].SetFillColorAlpha(color_list[pdf],0.3)
        else:
            hist_diff[pdf].SetFillColor(color_list[pdf])
            #hist_diff[pdf].SetFillColor(color_list[pdf])
            hist_diff[pdf].SetFillStyle(MarketFill_list[pdf])
        if "CT10" not in pdf:
            mg_2.Add(hist_diff[pdf])
        hist_diff[pdf].SetLineWidth(2)


    pad_1.cd(0)
    mg.Draw("apz")
    #mg.Draw("pX")

    ROOT.gPad.RedrawAxis()
    c3.Update()
    gr1 = ROOT.TGraph(2)
    gr1.SetPoint(0, -1, 0)
    gr1.SetPoint(1, 1000, 0)
    gr1.SetLineStyle(7)
    ROOT.gPad.RedrawAxis()
    gr1.Draw("same")
    legend.Draw("same")
    mg_2.GetYaxis().SetRangeUser(-0.07, 0.07)
   
    print(var)
    if var == "pt":
        mg_2.GetXaxis().SetTitle("p_{T}^{l#nu} [GeV]")
        mg.GetXaxis().SetRangeUser(0,600)
        mg_2.GetXaxis().SetRangeUser(0,600)

        if Ai in ["0","2","3","4"]: mg.GetYaxis().SetRangeUser(-0.1, 1.15)
        elif Ai in "1": mg.GetYaxis().SetRangeUser(-0.07, 0.13)
        elif Ai in ["5","6","7","4"]:
            mg.GetYaxis().SetRangeUser(-0.1, 0.1) 
            mg_2.GetYaxis().SetRangeUser(-0.04, 0.04)

        elif Ai in "CS":
            mg.GetYaxis().SetRangeUser(0,mg.GetYaxis().GetXmax()*3/2)
            mg_2.GetYaxis().SetRangeUser(-37, 7)



    elif var == "y":
        mg_2.GetXaxis().SetTitle("|y^{l#nu}|")
        #mg.GetXaxis().SetRangeUser(-0.2,3.8)
        #mg_2.GetXaxis().SetRangeUser(-0.2,3.8)
        ymax = mg.GetYaxis().GetXmax();
        ymin = min(-0.01, mg.GetYaxis().GetXmin())
        dy0 = ymax - ymin
        dy1 = frac_insert / (1 - frac_insert) * dy0
        dy_tot = dy1 + dy0
        mg.GetYaxis().SetRangeUser(ymin, ymin + dy_tot)
        mg_2.GetYaxis().SetRangeUser(-0.031, 0.031)
        print("pint")
        if Ai in ["4"]:
            mg_2.GetYaxis().SetRangeUser(-0.05, 0.1)
            #if "WPenu" in channel: 
            #    mg_2.GetYaxis().SetRangeUser(-0.2, 0.1)
        elif Ai in ["0","1","2","3","5","6","7"]:
            mg_2.GetYaxis().SetRangeUser(-0.01, 0.01)
            if "WPenu" in channel:
            #    if Ai in ["0","3"]:
                mg_2.GetYaxis().SetRangeUser(-0.005, 0.005)

        elif Ai in "CS":
            #mg_2.GetYaxis().SetRangeUser(-250, 50)
            mg_2.GetYaxis().SetRangeUser(0.93, 1.15)

            #if "WPenu" in channel:
            #    mg_2.GetYaxis().SetRangeUser(-1500, 500)


    if Ai not in "CS":    
        mg.GetYaxis().SetTitle("A_{%s}"%Ai)
        mg.GetYaxis().SetLabelSize(0.05)
        mg_2.GetYaxis().SetTitle("A_{%s}(CT10)-A_{%s}"%(Ai,Ai))
        mg.GetYaxis().SetTitleSize(0.055)
        mg.GetYaxis().SetTitleOffset(0.97)

    else:
        mg.GetYaxis().SetLabelSize(0.05)
        mg.GetYaxis().SetLabelOffset(0.0)
        if var in "y":
            mg.GetYaxis().SetTitle("d#sigma/dy")
            mg_2.GetYaxis().SetTitle("#sigma)/#sigma(CT10)")

        if var in "pt":
            mg.GetYaxis().SetTitle("d#sigma/dp_{T}")
            mg_2.GetYaxis().SetTitle("#sigma)/#sigma(CT10)")
        mg.GetYaxis().SetTitleSize(0.065)
        mg.GetYaxis().SetTitleOffset(0.84)
        
    mg_2.GetXaxis().SetLabelSize(0.08)
    mg_2.GetXaxis().SetTitleSize(0.084)

    mg_2.GetYaxis().SetLabelSize(0.08)
    mg_2.GetYaxis().SetTitleSize(0.1)
    mg_2.GetYaxis().SetTitleOffset(0.55)

    pad_2.cd(0)
    mg_2.Draw("apz")
    gr2 = ROOT.TGraph(2)
    if Ai not in "CS":   
        gr2.SetPoint(0, -1, 0)
        gr2.SetPoint(1, 1000, 0)
    else:   
        gr2.SetPoint(0, -1, 1)
        gr2.SetPoint(1, 1000, 1)
    gr2.SetLineStyle(7)
    ROOT.gPad.RedrawAxis()
    gr2.Draw("same")
    
    if logXex:
        pad_1.SetLogx()
        pad_2.SetLogx()
    pad_1.Update()
    pad_2.Update()
    pad_1.RedrawAxis()
    pad_2.RedrawAxis()

    c3.Draw()
    save_name = "predict_"+var+"_A"+Ai
    c3.SaveAs(save_dir+"_"+version+"/"+save_name+".pdf")
    c3.SaveAs(save_dir+"_"+version+"/"+save_name+".png")
        #c3.SaveAs("testplot.pdf")
        #