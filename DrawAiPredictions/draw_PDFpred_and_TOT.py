import ROOT
from filereader.filereader import CalcArefAndUns,readArefAndUnsFromFile,fillAiHist,fillAiHist_v1
#file_path = files_path = "/eos/user/g/gtolkach/WAi/WMenu_WAi_pt_new_v1.0_CC_v2.0/pseudoData/m00_y00/PDF.txt"
#file_path = files_path = "/eos/user/g/gtolkach/WAi/WMenu_WAi_y_CT10NLO_v1.0_CC_v2.0/pseudoData/pt00_m00/PDF.txt"

#emunus
#file_path = "/eos/user/g/gtolkach/WAi/test_123/WMenu_WAi_y_CT10NLO_v1.0_CC_v1.0/pseudoData/pt00_m00/STAT.txt" 
#graph_from_file = readArefAndUnsFromFile(4,file_path, "y")
#files_path_CT18NNLO = "/eos/user/g/gtolkach/WAi/prediction/CC_WMenu_WAi_y_CT18NNLO_v1.0/mainScanTruth/"
#files_path_CT10NLO = "/eos/user/g/gtolkach/WAi/prediction/CC_WMenu_WAi_y_CT10NLO_v1.0/mainScanTruth/"
#files_path_NNPDF = "/eos/user/g/gtolkach/WAi/prediction/CC_WMenu_WAi_y_NNPDF40_v1.0/mainScanTruth/"
#files_path_MSHT = "/eos/user/g/gtolkach/WAi/prediction/CC_WMenu_WAi_y_MSHT_v1.0/mainScanTruth/"

#eplus
file_path = "/eos/user/g/gtolkach/WAi/test_123/WPenu_WAi_y_CT10NLO_v1.0_CC_v4.0/pseudoData/pt00_m00/STAT.txt" 
graph_from_file = readArefAndUnsFromFile(4,file_path, "y")
files_path_CT18NNLO = "/eos/user/g/gtolkach/WAi/prediction/CC_WPenu_WAi_y_CT18NNLO_v1.0/mainScanTruth/"
files_path_CT10NLO = "/eos/user/g/gtolkach/WAi/prediction/CC_WPenu_WAi_y_CT10NLO_v1.0/mainScanTruth/"
files_path_CT10NLO = "/eos/user/g/gtolkach/WAi/prediction/CC_WPenu_WAi_y_CT10NLO_v2.0/mainScanTruth/"
files_path_NNPDF = "/eos/user/g/gtolkach/WAi/prediction/CC_WPenu_WAi_y_NNPDF40_v1.0/mainScanTruth/"
files_path_MSHT = "/eos/user/g/gtolkach/WAi/prediction/CC_WPenu_WAi_y_MSHT_v1.0/mainScanTruth/"

Ai_n = 8
var = "y"
save_dir = "plotsAref/prediction/CT10_CT18_NNPDF40/y"
channel = "WPenu"
logXex = False
version = "v1.0"
    
    
    
AiRef_CT18, AiRefErr_CT18, pdf_err_hi_CT18, pdf_err_lo_CT18,  pdf_scale_err_hi_CT18, pdf_scale_err_lo_CT18  = CalcArefAndUns(files_path_CT18NNLO)
AiRef_CT10, AiRefErr_CT10, pdf_err_hi_CT10, pdf_err_lo_CT10,  pdf_scale_err_hi_CT10, pdf_scale_err_lo_CT10   = CalcArefAndUns(files_path_CT10NLO)
AiRef_NNPDF, AiRefErr_NNPDF, pdf_err_hi_NNPDF, pdf_err_lo_NNPDF,  pdf_scale_err_hi_NNPDF, pdf_scale_err_lo_NNPDF   = CalcArefAndUns(files_path_NNPDF)
AiRef_MSHT, AiRefErr_MSHT, pdf_err_hi_MSHT, pdf_err_lo_MSHT,  pdf_scale_err_hi_MSHT, pdf_scale_err_lo_MSHT   = CalcArefAndUns(files_path_MSHT)


_, _, pdf_CT10, _, _,total_pdf_CT10 = fillAiHist_v1(AiRef_CT10, AiRefErr_CT10,pdf_err_hi_CT10, pdf_err_lo_CT10, pdf_scale_err_hi_CT10, pdf_scale_err_lo_CT10,var)
_, _, pdf_CT18, _, _, total_pdf_CT18 = fillAiHist_v1(AiRef_CT18, AiRefErr_CT18,pdf_err_hi_CT18, pdf_err_lo_CT18,pdf_scale_err_hi_CT18, pdf_scale_err_lo_CT18,var)

_, _, pdf_NNPDF, _, _, total_pdf_NNPDF = fillAiHist_v1(AiRef_NNPDF, AiRefErr_NNPDF,pdf_err_hi_NNPDF, pdf_err_lo_NNPDF, pdf_scale_err_hi_NNPDF, pdf_scale_err_lo_NNPDF,var)

_, _, pdf_MSHT, _, _, total_pdf_MSHT = fillAiHist_v1(AiRef_MSHT, AiRefErr_MSHT, pdf_err_hi_MSHT, pdf_err_lo_MSHT, pdf_scale_err_hi_MSHT, pdf_scale_err_lo_MSHT,var)

h_m = graph_from_file.Clone("m")
h_CT10 = total_pdf_CT10["A4"].Clone("CT10")
h_CT18 =  total_pdf_CT18["A4"].Clone("CT18")
h_NNPDF = total_pdf_NNPDF["A4"].Clone("NNPDF")
h_MSHT = total_pdf_MSHT["A4"].Clone("MSHT")

c = ROOT.TCanvas("c1","c1",1024,768)
mg = ROOT.TMultiGraph()
ROOT.gPad.SetTickx()
ROOT.gPad.SetTicky()
ROOT.gStyle.SetEndErrorSize(7)


legend = ROOT.TLegend(0.15, 0.65,0.35,0.85,"", "NDC")
header = ""    
legend.SetHeader(header)

legend.SetFillStyle(0)
legend.SetTextSize(0.04)
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetTextFont(42)

hist_name = ["total unc. from A4 measurement", "pdf unc. from CT10", "pdf unc. from CT18NNLO", "pdf unc. from MSHTNNLO20", "pdf unc. from NNPDF4.0NNLO"]
hist = {hist_name[0]:h_m, hist_name[1]:h_CT10, hist_name[2]:h_CT18, hist_name[3] :h_MSHT ,hist_name[4]: h_NNPDF}
color_list = {hist_name[0]: 1,hist_name[1]:209, hist_name[2]: 2, hist_name[3]:ROOT.kPink+8, hist_name[4]:ROOT.kAzure+1,  }
MarketStyle_list = {hist_name[0]: 8, hist_name[1]:25, hist_name[2]: 32,hist_name[3]:31, hist_name[4] : 4}

for pdf in hist_name:
    hist[pdf].SetTitle("")
    #hist[pdf].SetMarkerColor(color_list[pdf])
    #hist[pdf].SetLineColor(color_list[pdf])
    hist[pdf].SetLineColor(1)
    #hist[pdf].SetMarkerStyle(MarketStyle_list[pdf])
    #hist[pdf].SetMarkerSize(1.5)
    #hist[pdf].SetLineWidth(2)
    #hist[pdf].SetFillStyle(3004)
    if "measurement" in pdf:
        hist[pdf].SetFillColorAlpha(color_list[pdf],0.1)
    else:
        hist[pdf].SetFillColorAlpha(color_list[pdf],0.2)
    #hist[pdf].SetFillColorAlpha(color_list[pdf],0.3)
    if "CT10"  not in pdf:
        print(pdf)
        mg.Add(hist[pdf])
        legend.AddEntry(hist[pdf], pdf,"f")


mg.GetYaxis().SetTitle("Uncertainty in A_{4}")
mg.GetYaxis().SetRangeUser(-0.2, 0.20)
mg.GetXaxis().SetTitle("|y^{l#nu}|")
#mg.GetYaxis().SetLabelSize(0.05)
mg.GetYaxis().SetTitleSize(0.055)
mg.GetYaxis().SetTitleOffset(0.7)


mg.Draw("a2")
legend.Draw("same")
c.Draw()
c.SaveAs("Uns_for_all_predict_and_meas_plus.png")