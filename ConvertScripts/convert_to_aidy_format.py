import ROOT
from math import acos, cos, sin
import numpy as np
from array import array 
from tqdm import tqdm
from reweighting.reweighting import GetSF
from datetime import datetime
import os


m_nAi = 8
# Function from AIDY (c++ --> python)
# https://gitlab.cern.ch/waiteam/aidy/-/blob/master/source/aidy/src/ScanCommon.cxx#L1169
# function for calculating partial polynomials
#--------------------------------------------------------------------------
def polynAi(index, cosThetaCS, phiCS):
    temp = 1

    sinThetaCS = 0
    if 1.0 - cosThetaCS * cosThetaCS > 0: sinThetaCS = (1.0 - cosThetaCS * cosThetaCS)**0.5
    sin2ThetaCS = 2 * sinThetaCS * cosThetaCS

    if index == m_nAi:
        temp = 1. + cosThetaCS * cosThetaCS
    elif index == 0:
        temp = 0.5 * (1. - 3. * cosThetaCS * cosThetaCS)
    elif index == 1:
        temp = sin2ThetaCS * cos(phiCS)
    elif index == 2:
        temp = 0.5 * sinThetaCS * sinThetaCS * cos(2 * phiCS)
    elif index == 3:
        temp = sinThetaCS * cos(phiCS)
    elif index == 4:
        temp = cosThetaCS
    elif index == 5:
        temp = sinThetaCS * sinThetaCS * sin(2 * phiCS)
    elif index == 6:
        temp = sin2ThetaCS * sin(phiCS)
    elif index == 7:
        temp = sinThetaCS * sin(phiCS)

    return temp


def convert_aidy(var = "pt", inFilePath = "events.root", treeName = "t", outPath = "",bin_mode = 1):
#Hey Grigorii, for pt scan it's pt/m/y: 8/13/6 and for y scan pt/m/y: 3/13/10
    A4scale = 1
    current = datetime.now()
    out_name = "output/%s%s%s_%s_%s_%s"%(current.day, current.month, current.year, current.hour, current.minute, current.second)
    path = os.getcwd()
    print ("current path %s" % path)
    try:
        os.mkdir(out_name)
        out_name+="/Nominal"
        os.mkdir(out_name)

    except OSError:
        print ("The %s directory could not be created" % out_name)
    else:
        print ("The directory was successfully created%s " % out_name)

    out_name +="/hists.root" 
    print ("out file path %s " % out_name)

    f_in = ROOT.TFile(inFilePath)
    tree = f_in.Get(treeName)
    f_out = ROOT.TFile(out_name, "recreate")
    
    if "pt" in var:
        #8 pT

        if bin_mode == 1:
            PTBins_list = [0., 8., 17., 27., 40., 55., 75., 110., 150., 210., 600.]
        elif bin_mode == 2:
            PTBins_list = [0.0, 5.0, 10.0, 15.0, 20.0, 25.0, 30.0, 35.0, 40.0, 45.0, 50.0, 55.0, 60.0, 65.0, 70.0,
             75.0, 80.0, 85.0, 90.0, 95.0, 100.0, 105.0, 110.0, 115.0, 120.0, 125.0, 130.0, 135.0]

        PTBins = array("d" , PTBins_list )
        NBinsPT = len(PTBins_list)-1
        #13 M
        MBins_list = [0, 6000]
        MBins = array("d" , MBins_list )
        NBinsM = len(MBins_list)-1
        #6 Y
        YBins_list = [0, 10]
        YBins = array("d" , YBins_list )
        NBinsY = len(MBins_list)-1

    h_ptll_mll_yll = ROOT.TH3D("ptll_mll_yll", "ptll_mll_yll", NBinsPT, PTBins, NBinsM, MBins, NBinsY, YBins)
    h_ptll_mll_yll_wi = {}
    h_ptll_mll_yll_wiwi = {}

    for i in range(m_nAi):
        h_ptll_mll_yll_wi[i] =  ROOT.TH3D("ptll_mll_yll_w%s"%i, "ptll_mll_yll_w%s"%i, NBinsPT, PTBins, NBinsM, MBins, NBinsY, YBins)
        h_ptll_mll_yll_wiwi[i] =  ROOT.TH3D("ptll_mll_yll_w%sw%s"%(i,i), "ptll_mll_yll_w%sw%s"%(i,i), NBinsPT, PTBins, NBinsM, MBins, NBinsY, YBins)

    evt = tree.GetEntries()
    sf = False
    if sf:
        w_SF = GetSF(evt)
        print(w_SF)
    else:
        w_SF =1.0
    evt = 1000000
    for i in tqdm(range(evt)):
        tree.GetEntry(i)
        w = 1.0
        w *=w_SF
        if bin_mode == 1:
            cosThetaCS = tree.CosThetaCS
            phiCS = tree.PhiCS
            p4_W = tree.p4_lep+ tree.p4_nu
            yll = abs(p4_W.Rapidity())
            ptll = p4_W.Pt()
            mll = p4_W.M()
        else:
            cosThetaCS = tree.CosThetaCS_truth
            phiCS = tree.PhiCS_truth
            yll = abs(tree.boson_rapidity_truth)
            ptll = tree.boson_pT_truth/1000
            mll = tree.boson_mT_truth

        h_ptll_mll_yll.Fill(ptll, mll, yll, w)
        for Ai in range(m_nAi):
            sign = 1
            wt_Ai = polynAi(Ai, cosThetaCS, phiCS)
            if i == 4 and yll > 3.5:
                wt_Ai *= A4scale
            h_ptll_mll_yll_wi[Ai].Fill(ptll, mll, yll, sign*w * wt_Ai)
            h_ptll_mll_yll_wiwi[Ai].Fill(ptll, mll, yll, w * wt_Ai * wt_Ai)

    f_out.Write()
    f_out.Close()



if __name__ == "__main__":
    convert_aidy()