import lhapdf
import ROOT
class PDFTool():
    def __init__(self,PDFset="CT10", variation="Nominal"):
        self.PDFset = PDFset
        self.variation = variation
        self.pdfSetInMC = "CT10"
        self.mcSet = lhapdf.mkPDF("%s/0"%self.pdfSetInMC)
        if self.pdfSetInMC in self.PDFset:
            self.nominalSet = self.mcSet
        else:
            self.nominalSet = lhapdf.mkPDF("%s/0"%PDFset)
        self.NP_number = 0
        self.pdfEigen = 0
        self.pdf_sets = {"CT10":52, "CT18NNLO":58}
        self.branchName = ""
        self.definePDFset()
        if "PDF" in variation:
            self.pdfSetinVar = self.GetPDFSet()
        else:
            self.pdfSetinVar = 0

    def pdf_isHi(self, EV):
        if "NNPDF" in self.PDFset:
            return True
        else:
            if (EV-1) % 2 == 0:
                return True
            else:
                return False 

    def get_NP_number_pdf(self, EV):
        nEVs = self.pdf_sets[self.PDFset]
        NP_number = 0
        for i in range(1,nEVs+1):
            if self.pdf_isHi(i):
                NP_number+=1
            print("on i = %s, NP_number = %s"%(i, NP_number))
            if (i == EV): break
        return NP_number

    def definePDFset(self):
        if "PDF" in self.variation:
            self.pdfEigen = int(self.variation.split("PDF")[1])
            if self.pdfEigen > self.pdf_sets[self.PDFset]:
                print("ERROR: pdfEigen > MAX PDF set number")
                exit(1)
            
            self.NP_number = self.get_NP_number_pdf(self.pdfEigen)
            self.branchName = "PDF"+str(self.NP_number)
            isHiEV = self.pdf_isHi(self.pdfEigen)
            if isHiEV:
                self.branchName+="Hi"
            else:
                self.branchName+="Lo"
            print("branchName = %s"%self.branchName)

    def GetPDFSet(self):
            return lhapdf.mkPDF("%s/%s"%(self.PDFset, self.pdfEigen))

    def GetNominalPDFWeight(self, pdf_id1, pdf_id2,pdf_x1, pdf_x2, pdf_scale):
        w = 1.0
        if self.nominalSet != self.mcSet:
            w = lhapdf.weightxxQ(pdf_id1, pdf_id2,
                                  pdf_x1, pdf_x2,
                                  pdf_scale, self.mcSet,
                                  self.nominalSet)
        if w != w or w > 10e30 or w < -10e30:
            print("ERROR: w = %s"%w)
            w = 0
        return w

    def GetPDFWeight(self, pdf_id1, pdf_id2,pdf_x1, pdf_x2, pdf_scale):
        weightNom = self.GetNominalPDFWeight(pdf_id1, pdf_id2,pdf_x1, pdf_x2, pdf_scale)
        scale = ROOT.Math.gaussian_quantile(1 - 0.1 / 2, 1)
        weight = 1

        if "CT18" in self.PDFset:
            delta = lhapdf.weightxxQ(pdf_id1, pdf_id2,
                                     pdf_x1, pdf_x2,pdf_scale,
                                     self.nominalSet, self.pdfSetinVar)

            delta_noscale = delta
            delta = 1 + (delta - 1)/scale
            weight = weightNom * delta

        else:
            weight = lhapdf.weightxxQ(pdf_id1, pdf_id2,
                                    pdf_x1, pdf_x2,pdf_scale,
                                    self.mcSet, self.pdfSetinVar)
      

        if "CT10" in self.PDFset:
            weight = 1 + (weight - 1)/scale
        
        return weight