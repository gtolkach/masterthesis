def GetSF(Ngen, channel = "W+"):
    lumi = 335.0 #pb^-1 low-mu data
    CrossSection = 0.0 
    if "+" in channel:
        #CrossSection = 11231.0 #pb <-- from Powheg-BOX-V1  NLO
        CrossSection = 10144.607917817826 #pb <-- from Powheg-BOX-V1  LO
    w = lumi*CrossSection/int(Ngen)
    print("SF = lumi*CrossSection/Ngen = %s*%s/%s = %s"%(lumi,CrossSection, Ngen,  w))
    return w