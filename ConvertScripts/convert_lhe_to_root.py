import ROOT as r
from array import array
from tqdm import tqdm
from ROOT import TLorentzVector, TVector3
import math

# Function from AIDY (c++ --> python)
# https://gitlab.cern.ch/waiteam/aidy/-/blob/master/source/aidy/src/ScanCommon.cxx#L896
def GetCosThetaRF(lepM, lepP, frame):

    #Calculates cos(Theta*) in the Boson Rest Frame.
    #boost negative lepton in Z-rest frame, calls appropriative
    #Frame(CS,HX,PX,GJ) and calculates angle  between ZAxis and the boosted
    #negative lepton. return cos(Theta*)
    boostedLep = TLorentzVector()
    Z = TLorentzVector()
    boostedLep = lepM
    Z = lepM + lepP
    boostedLep.Boost(-Z.BoostVector())
    RFAxis, xAxis, yAxis = RFrame(Z, frame)
    return math.cos(boostedLep.Angle(RFAxis))


# Function from AIDY (c++ --> python)
# https://gitlab.cern.ch/waiteam/aidy/-/blob/master/source/aidy/src/ScanCommon.cxx#L849
def RFrame(Z,frame):
  #---------------------------------------------------------------------------
  # boost event into the boson rest frame  sets the frame axis
  # input TLorentzVector Z boson, string frame name
  # output RFaxis, xAxis, yAxis
  # return false if sanity checks fail, true otherwise
    RFAxis = TVector3()
    yAxis = TVector3()
    xAxis = TVector3()
    m_GeV = 1000
    sqrtS = 13
    isGood = True
    p1 = TLorentzVector()
    p2 = TLorentzVector()
    sign = abs(Z.Z()) / Z.Z()
    ProtonMass = 0.938272 / m_GeV
    BeamEnergy = sqrtS* 1000/2/m_GeV #// MeV
    p1.SetPxPyPzE(0, 0, sign * BeamEnergy, (BeamEnergy * BeamEnergy + ProtonMass * ProtonMass)**0.5)
    p2.SetPxPyPzE(0, 0, -1 * sign * BeamEnergy, (BeamEnergy * BeamEnergy + ProtonMass * ProtonMass)**0.5 ) 
    p1.Boost(-Z.BoostVector())
    p2.Boost(-Z.BoostVector())
    if "CS" in frame:
        RFAxis = ((p1.Vect()).Unit() - (p2.Vect()).Unit()).Unit()
    else:
        print("ERROR")
        exit(1)
                  
    yAxis = (p1.Vect().Unit()).Cross((p2.Vect().Unit()))
    yAxis = yAxis.Unit()
    xAxis = yAxis.Cross(RFAxis)
    xAxis = xAxis.Unit()

    return RFAxis, xAxis, yAxis

# Function from AIDY (c++ --> python)
# https://gitlab.cern.ch/waiteam/aidy/-/blob/master/source/aidy/src/ScanCommon.cxx#L916

def GetPhiRF(lepM,  lepP,  frame):
    #------------------------------------------------------------------------
    # Calculates phi in the Boson rest Frame.
    # boosts the negative lepton into to boson rest frame,
    # calls appropriative Frame(CS,GJ,PX,HX)  and  calculates
    # the angle of the boosted lepton, projected onto the x,y plane.
    # retrun phi
    boostedLep = TLorentzVector()
    Z = TLorentzVector()

    boostedLep = lepM
    Z = lepM + lepP
    boostedLep.Boost(-Z.BoostVector())
    RFAxis, xAxis, yAxis = RFrame(Z, frame)
    #atan2 Principal arc tangent of y/x, in the interval [-pi,+pi] radians.
    phi = math.atan2((boostedLep.Vect() * yAxis), (boostedLep.Vect() * xAxis))
    doMod = 1
    if phi < 0 and doMod:
        phi = phi + 2 * math.pi

    return phi

def convert_lhe(fname_in, fname_out="events.root"):
    frame = "CS"        
    # 24  W+
    # -24 W-


    IDW = 24

    events = []
    event = ""
    in_event = False
    with open(fname_in, "r") as fhin:
        iline = 0
        for line in tqdm(fhin):

            if in_event and line.startswith("<"):
                in_event = False
                events.append(event)
                event = ""
            if in_event:
                event += line
            if line.startswith("<event>"):
                in_event = True
        if event:
            events.append(event)

    f1 = r.TFile(fname_out, "recreate")
    t1 = r.TTree("t","t")

    PDG_id1 = array( 'i', [ 0 ] )
    PDG_id2 = array( 'i', [ 0 ] )
    PDG_x1 = array( 'd', [ 0 ] )
    PDG_x2 = array( 'd', [ 0 ] )
    PDG_Q = array( 'd', [ 0 ] )
    rapidity_W = array( 'd', [ 0 ] )
    pt_W = array( 'd', [ 0 ] )
    CosThetaCS = array( 'd', [ 0 ] )
    PhiCS = array( 'd', [ 0 ] )

    p4_lep = r.TLorentzVector()
    p4_nu = r.TLorentzVector()
    p4_W = r.TLorentzVector()
    
    t1.Branch("p4_lep","TLorentzVector",p4_lep)
    t1.Branch("p4_nu","TLorentzVector",p4_nu)
    t1.Branch("p4_W","TLorentzVector",p4_W)

    t1.Branch("PDG_id1",PDG_id1, "PDG_id1/I")
    t1.Branch("PDG_id2",PDG_id2, "PDG_id2/I")
    t1.Branch("PDG_x1",PDG_x1, "PDG_x1/D")
    t1.Branch("PDG_x2",PDG_x2, "PDG_x2/D")
    t1.Branch("PDG_Q",PDG_Q, "PDG_Q/D")
    t1.Branch("rapidity_W",rapidity_W, "rapidity_W/D")
    t1.Branch("pt_W",pt_W, "pt_W/D")
    t1.Branch("CosThetaCS",CosThetaCS, "CosThetaCS/D")
    t1.Branch("PhiCS",PhiCS, "PhiCS/D")



    for ievt,evt in tqdm(enumerate(events)):
        particle_lines = evt.splitlines()[1:]
        n_el = 0
        n_nu = 0
        for particle_line in particle_lines:
            parts = particle_line.split()
            if "#pdf" in  parts:
                #pdf id1 id2 x1 x2 xmufact xf1 xf2
                PDG_id1[0] = int(parts[1])
                PDG_id2[0] = int(parts[2])
                PDG_x1[0] = float(parts[3])
                PDG_x2[0] = float(parts[4])
                PDG_Q[0] = float(parts[5])
            else:
                evt_pdgid = int(parts[0])
                evt_px, evt_py, evt_pz, evt_e = map(float,parts[6:10])
                if abs(evt_pdgid) == 11:
                    p4_lep.SetPxPyPzE(evt_px, evt_py, evt_pz, evt_e)
                    n_el+=1
                elif abs(evt_pdgid) == 12:
                    n_nu+=1
                    p4_nu.SetPxPyPzE(evt_px, evt_py, evt_pz, evt_e)
                elif abs(evt_pdgid) == 24:
                    p4_W.SetPxPyPzE(evt_px, evt_py, evt_pz, evt_e)
        #if ievt % 100000 == 0:
        #    print(ievt,"/",len(events))
        if IDW == 24:
            lepM = p4_nu
            lepP = p4_lep
        elif IDW == -24:
            lepM = p4_lep
            lepP = p4_nu
        
        evt_CosThetaCS = float(GetCosThetaRF(lepM.Clone("Vec1"), lepP.Clone("Vec2"), frame))
        evt_PhiCS = float(GetPhiRF(lepM.Clone("Vec12"), lepP.Clone("Vec22"), frame))
        evt_pt =  float((p4_lep + p4_nu).Pt())
        evt_Y = float((p4_lep + p4_nu).Rapidity())

        #print("Pt",abs(p4_W.Pt()),abs((p4_lep + p4_nu).Pt()) )
        #print("Rapidity",abs(p4_W.Rapidity()),abs((p4_lep + p4_nu).Rapidity()) )

        CosThetaCS[0] = evt_CosThetaCS
        PhiCS[0] = evt_PhiCS
        pt_W[0] = evt_pt
        rapidity_W[0] = evt_Y
        #print(evt_Y)
        t1.Fill()    

    #t1.Print()
    t1.Write()
    f1.Write()
    f1.Close()

if __name__ == "__main__":
    #file_path = "/afs/cern.ch/work/g/gtolkach/application/POWHEG-BOX-V1/W/testrun-wp-lhc/pwgevents.lhe"
    file_path = '/afs/cern.ch/work/g/gtolkach/application/MG5_aMC_v2_6_7/test_lowest_wpluse/Events/run_01/unweighted_events.lhe'
    convert_lhe(file_path)