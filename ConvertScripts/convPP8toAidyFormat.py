from convert_to_aidy_format import convert_aidy

def main():
    #file_path = "/eos/user/d/dponomar/Storage/Science/Wai/microtree_example/PDF_reweight/v20220825/mc16_13TeV.361103.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wminusenu.STDM4.e3601_s3126_r10244+r11165_p3665.root"
    file_path = "/eos/user/d/dponomar/Storage/Science/Wai/microtree_example/PDF_reweight/v20220718/mc16_13TeV.361100.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusenu.e3601_s3126_r10244+r11165_p3665.root"
    convert_aidy(inFilePath = file_path, treeName = "MicroTree/HWWTree_ee",bin_mode = 2)


if __name__ == "__main__":
    main()