import ROOT
from CalculatePDFUncFromFile import  CalculatePDFUnc
def DrawRatioPDF(pdfset_name = "",var ="",channel = ""):
    hist_name = "h_"+var
    _, hi, lo = CalculatePDFUnc("rootfiles/"+channel+"_100k_"+pdfset_name+".root",hist_name, pdfset_name)
    _, hi_Ai, lo_Ai = CalculatePDFUnc("rootfiles/"+channel+"_100k_"+pdfset_name+"_A4_A9.root",hist_name, pdfset_name)
    _, hi_A4, lo_A4 = CalculatePDFUnc("rootfiles/"+channel+"_100k_"+pdfset_name+"_A4.root",hist_name, pdfset_name)
    _, hi_A9, lo_A9 = CalculatePDFUnc("rootfiles/"+channel+"_100k_"+pdfset_name+"_A9.root",hist_name, pdfset_name)
    _, hi_A9sf10, lo_A9sf10 = CalculatePDFUnc("rootfiles/"+channel+"_100k_"+pdfset_name+"_A9SF10.root",hist_name, pdfset_name)
    _, hi_A4sf10, lo_A4sf10 = CalculatePDFUnc("rootfiles/"+channel+"_100k_"+pdfset_name+"_A4SF10.root",hist_name, pdfset_name)
    _, hi_Aisf10, lo_Aisf10 = CalculatePDFUnc("rootfiles/"+channel+"_100k_"+pdfset_name+"_A4_A9SF10.root",hist_name, pdfset_name)

    if "mt" in hist_name:
        h_A4_A9 = ROOT.TH1D("h_mt_A4_A9","; m_{T}[GeV]; #Delta PDF_{prof.}/#Delta PDF_{base}",25 ,  50 , 100)
        h_A9 = ROOT.TH1D("h_mt_A9","; m_{T}[GeV]; #Delta PDF_{prof.}/#Delta PDF_{base}",25 ,  50 , 100)
        h_A4 = ROOT.TH1D("h_mt_A4","; m_{T}[GeV]; #Delta PDF_{prof.}/#Delta PDF_{base}",25 ,  50 , 100)
        h_A4sf10 = ROOT.TH1D("h_mt_A4sf10","; m_{T}[GeV]; #Delta PDF_{prof.}/#Delta PDF_{base}",25 ,  50 , 100)
        h_A9sf10 = ROOT.TH1D("h_mt_A9sf10","; m_{T}[GeV]; #Delta PDF_{prof.}/#Delta PDF_{base}",25 ,  50 , 100)
        h_A4_A9sf10 = ROOT.TH1D("h_mt_A4_A9sf10","; m_{T}[GeV]; #Delta PDF_{prof.}/#Delta PDF_{base}",25 ,  50 , 100)
    elif "pt" in hist_name:
        h_A4_A9 = ROOT.TH1D("h_pt_A4_A9","; p_{T}[GeV]; #Delta PDF_{prof.}/#Delta PDF_{base}",45 ,  0 , 90)
        h_A9 = ROOT.TH1D("h_pt_A9","; p_{T}[GeV]; #Delta PDF_{prof.}/#Delta PDF_{base}",45 ,  0 , 90)
        h_A4 = ROOT.TH1D("h_pt_A4","; p_{T}[GeV]; #Delta PDF_{prof.}/#Delta PDF_{base}",45 ,  0 , 90)
        h_A4sf10 = ROOT.TH1D("h_pt_A4sf10","; p_{T}[GeV]; #Delta PDF_{prof.}/#Delta PDF_{base}",45 ,  0 , 90)
        h_A9sf10 = ROOT.TH1D("h_pt_A9sf10","; p_{T}[GeV]; #Delta PDF_{prof.}/#Delta PDF_{base}",45 ,  0 , 90)
        h_A4_A9sf10 = ROOT.TH1D("h_pt_A4_A9sf10","; p_{T}[GeV]; #Delta PDF_{prof.}/#Delta PDF_{base}",45 ,  0 , 90)

    #h_pdf_error = ROOT.TGraphAsymmErrors(h_central)
    list_res = [0,0,0,0,0,0]
    for ibin in hi:
        ratio_Ai = (hi_Ai[ibin]+lo_Ai[ibin])/(hi[ibin]+lo[ibin])
        ratio_A4 = (hi_A4[ibin]+lo_A4[ibin])/(hi[ibin]+lo[ibin])
        ratio_A9 = (hi_A9[ibin]+lo_A9[ibin])/(hi[ibin]+lo[ibin])
        ratio_A4sf10 = (hi_A4sf10[ibin]+lo_A4sf10[ibin])/(hi[ibin]+lo[ibin])
        ratio_A9sf10 = (hi_A9sf10[ibin]+lo_A9sf10[ibin])/(hi[ibin]+lo[ibin])
        ratio_Aisf10 = (hi_Aisf10[ibin]+lo_Aisf10[ibin])/(hi[ibin]+lo[ibin])

        list_res[0]+=ratio_Ai
        list_res[1]+=ratio_A4
        list_res[2]+=ratio_A9
        list_res[3]+=ratio_Aisf10
        list_res[4]+=ratio_A4sf10
        list_res[5]+=ratio_A9sf10
        h_A4_A9.SetBinContent(ibin, ratio_Ai)
        h_A4.SetBinContent(ibin, ratio_A4)
        h_A4sf10.SetBinContent(ibin, ratio_A4sf10)
        h_A9.SetBinContent(ibin, ratio_A9)
        h_A9sf10.SetBinContent(ibin, ratio_A9sf10)
        h_A4_A9sf10.SetBinContent(ibin, ratio_Aisf10)

    for i in range(len(list_res)):
        list_res[i] = (1 - list_res[i]/len(hi))*100
    print("result")
    print(list_res)

    c = ROOT.TCanvas()
    ROOT.gStyle.SetOptStat(0)
    legend = ROOT.TLegend(0.52, 0.70,0.62,0.88,"", "NDC")
    legend.SetFillStyle(0)
    legend.SetTextSize(0.03)
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetTextFont(42)
    pdfset_name_legend = ""
    if "HERA" in pdfset_name:
        pdfset_name_legend = "HERAPDF2.0NNLO"
    elif "MSHT" in pdfset_name:
        pdfset_name_legend = "MSHT20NNLO"
    elif "NNPDF" in pdfset_name:
        pdfset_name_legend = "NNPDF4.0NNLO"


    else:
        pdfset_name_legend = pdfset_name 

    legend.AddEntry(h_A4, pdfset_name_legend+" + A_{4}")
    legend.AddEntry(h_A4sf10, pdfset_name_legend+" + A_{4} (sf = 10)")
    legend.AddEntry(h_A9, pdfset_name_legend+" + A_{9}")
    legend.AddEntry(h_A9sf10, pdfset_name_legend+" + A_{9} (sf = 10)")
    legend.AddEntry(h_A4_A9, pdfset_name_legend+" + A_{4} + A_{9}")
    legend.AddEntry(h_A4_A9sf10, pdfset_name_legend+" + A_{4} + A_{9} (sf = 10)")



    pt =  ROOT.TPaveText(0.13,0.83,0.35,0.88,"NDC")
    pt.SetFillColor(0)
    pt.SetTextAlign(12)
    #pt.AddText('#it{ATLAS} #bf{#bf{Work in Progress}}')
    if "WMenu" in channel:
        pt.AddText('#bf{#bf{W^{-}#rightarrow e^{-}#nu, #sqrt{S}=13 TeV}}')
    elif "WPenu" in channel:
        pt.AddText('#bf{#bf{W^{+}#rightarrow e^{+}#nu, #sqrt{S}=13 TeV}}')

    #pt.AddText('#bf{#bf{SR}}')
    #pt.AddText('#bf{#bf{2015}}')

    h_A4_A9.GetYaxis().SetRangeUser(0.2, 1.3)

    h_A4_A9.SetLineWidth(2)
    h_A4.SetLineWidth(2)
    h_A4sf10.SetLineWidth(2)
    h_A9sf10.SetLineWidth(2)
    h_A9.SetLineWidth(2)
    h_A4_A9sf10.SetLineWidth(2)


    h_A4.SetLineColor(1)
    h_A4sf10.SetLineColor(3)
    h_A9.SetLineColor(2)
    h_A9sf10.SetLineColor(6)
    h_A4_A9sf10.SetLineColor(7)


    h_A4_A9.Draw("L")
    h_A4.Draw("L same")
    h_A4sf10.Draw("L same")

    h_A9.Draw("L same")
    h_A9sf10.Draw("L same")
    h_A4_A9sf10.Draw("L same")

    legend.Draw("same")
    pt.Draw("'NDC' same")

    c.Draw()
    fig_name = pdfset_name+hist_name.split("h")[1]
    out_dir = "plots/"

    c.SaveAs(out_dir+channel+"_"+fig_name+".jpg")
    c.SaveAs(out_dir+channel+"_"+fig_name+".pdf")

channel = "WPenu"
DrawRatioPDF("NNPDF40","pt",channel)
DrawRatioPDF("HERAPDF20","pt",channel)
DrawRatioPDF("MSHT20","pt",channel)
DrawRatioPDF("CT18NNLO","pt",channel)

DrawRatioPDF("NNPDF40","mt",channel)
DrawRatioPDF("HERAPDF20","mt",channel)
DrawRatioPDF("MSHT20","mt",channel)
DrawRatioPDF("CT18NNLO","mt",channel)