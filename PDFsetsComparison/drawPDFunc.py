import ROOT
from CalculatePDFUncFromFile import  CalculatePDFUnc
hist_name = "h_pt"
central, hi, lo = CalculatePDFUnc("rootfiles/WMenu_100к_CT18NNLO.root",hist_name, "CT18")
central_Ai, hi_Ai, lo_Ai = CalculatePDFUnc("rootfiles/WMenu_100к_CT18NNLO_A4_A9.root",hist_name, "CT18")
delta = 5 
if "mt" in hist_name:
    h_central = ROOT.TH1D("h_mt_central","; m_{T}; N",25 ,  50 , 100)
    delta *= (100 - 50)/25
elif "pt" in hist_name:
    h_central = ROOT.TH1D("h_pt_central","; p_{T}; N",45 ,  0 , 90)
    delta *= (90 - 0)/45
lumi = 1.0
delta=delta*lumi
print(h_central)
h_pdf_error = ROOT.TGraphAsymmErrors(h_central)
h_pdf_error_Ai = ROOT.TGraphAsymmErrors(h_central)

n_bins = h_central.GetNbinsX()

for i in range(n_bins):
    h_central.SetBinContent(i+1, central[i+1]/delta)
    h_pdf_error.SetPointY(i, central[i+1]/delta)
    h_pdf_error.SetPointEYlow(i, lo[i+1]/delta)
    h_pdf_error.SetPointEYhigh(i, hi[i+1]/delta)
    h_pdf_error_Ai.SetPointY(i, central[i+1]/delta)
    h_pdf_error_Ai.SetPointEYlow(i, lo_Ai[i+1]/delta)
    h_pdf_error_Ai.SetPointEYhigh(i, hi_Ai[i+1]/delta)

print(h_central.Integral())
mg = ROOT.TMultiGraph()
h_pdf_error.SetFillColorAlpha(ROOT.kBlue,0.6)
mg.Add(h_pdf_error)
tmp = h_pdf_error_Ai.Clone("tmpCLONE")
tmp.SetFillColor(ROOT.kWhite)
mg.Add(tmp)

h_pdf_error_Ai.SetFillColorAlpha(ROOT.kRed,0.6)
#h_pdf_error_Ai.SetFillStyle(3002)
mg.Add(h_pdf_error_Ai)






c = ROOT.TCanvas()
#h_central.Draw("hist")

ROOT.gStyle.SetOptStat(0)
legend = ROOT.TLegend(0.6, 0.75,0.8,0.88,"", "NDC")
legend.SetFillStyle(0)
legend.SetTextSize(0.03)
legend.SetBorderSize(0)
legend.SetFillColor(0)
legend.SetTextFont(42)
legend.AddEntry(h_pdf_error, "#Delta_{PDF}", "f")
legend.AddEntry(h_pdf_error_Ai, "#Delta_{PDF} (A_{4} + A_{9} @ 335 pb^{-1})","f")



h_central.SetLineColor(1)
mg.GetYaxis().SetTitleOffset(1.4)

if "mt" in hist_name:
    mg.GetXaxis().SetTitle("M_{T} [GeV]")
    mg.GetYaxis().SetTitle("d\sigma/dM_{T} [pb/GeV]")
    mg.GetXaxis().SetRangeUser(50,100)
    #mg.GetXaxis().SetRangeUser(0.0,90000)

if "pt" in hist_name:
    mg.GetXaxis().SetTitle("p_{T}^{l#nu} [GeV]")
    mg.GetYaxis().SetTitle("d\sigma/ dp_{T} [pb/GeV]")
    mg.GetXaxis().SetRangeUser(0,90)
    mg.GetYaxis().SetRangeUser(0.0,10000)


mg.Draw("a2")


#h_pdf_error_Ai.Draw("a2 same")

h_central.Draw("hist same")
c.RedrawAxis()




c.Update()
mg.GetXaxis().SetNoExponent(0)
legend.Draw("same")
c.Draw()
c.SaveAs('test_1.jpg')
c.SaveAs('test_1.pdf')





