import ROOT

def CalculatePDFUnc(root_file_path = "",  hist_name = "", PDFset = ""):
    #root_file_path = "test_file.root"
    #hist_name = "h_mt"
    #PDFset = "CT10"
    PDFsets_dict = {"CT10":52, "CT18NNLO":58, "HERAPDF20":28,  "MSHT20":64, "NNPDF40":50}
    if "NNPDF" in PDFset:
        n_EV = PDFsets_dict[PDFset]
    else:
        n_EV = int(PDFsets_dict[PDFset]/2)

    hist_dir = {}
    file = ROOT.TFile(root_file_path)
    #print(file.ls())

    hist_central = file.Get(hist_name+"_central")
    n_bins = hist_central.GetNbinsX() 
    result_dict_central = {}
    result_dict_hi = {}
    result_dict_lo = {}

    print_dir_hi = {}
    print_dir_lo = {}

    for i in range(n_bins):
        result_dict_hi[i+1] = 0
        result_dict_lo[i+1] = 0
        result_dict_central[i+1] = hist_central.GetBinContent(i+1)

    for i in range(n_EV):
        hist_name_lo = hist_name+"_PDF"+str(i+1)+"Lo"
        hist_name_hi = hist_name+"_PDF"+str(i+1)+"Hi"
        if "NNPDF" in PDFset:
            hist_hi = file.Get(hist_name_hi)
        else:
            hist_hi = file.Get(hist_name_hi)
            hist_lo = file.Get(hist_name_lo)
        for k in range(n_bins):
            central = hist_central.GetBinContent(k+1)
            if "NNPDF" in PDFset:
                hi = hist_hi.GetBinContent(k+1)
                tmp_hi = abs(hi - central)
                tmp_lo = abs(hi - central)
            else:
                hi = hist_hi.GetBinContent(k+1)
                lo = hist_lo.GetBinContent(k+1)
                tmp_hi = max([0,hi - central, lo - central ])
                tmp_lo = max([0,central - hi,  central - lo])

            result_dict_hi[k+1] += tmp_hi**2
            result_dict_lo[k+1] += tmp_lo**2
    for i in range(n_bins):
        result_dict_hi[i+1] = result_dict_hi[i+1]**0.5
        result_dict_lo[i+1] = result_dict_lo[i+1]**0.5

        print_dir_hi[i+1] = 100*result_dict_hi[i+1]/result_dict_central[i+1]
        print_dir_lo[i+1] = 100*result_dict_lo[i+1]/result_dict_central[i+1]

    #for i in range(n_bins):
    #    hist_hi.SetBinContent(i+1, result_dict_hi[i+1])
    #    hist_lo.SetBinContent(i+1, result_dict_lo[i+1])


    mean_hi = 0
    mean_lo = 0
    for key in print_dir_hi:
        mean_hi+=print_dir_hi[key]
        mean_lo+=print_dir_lo[key]
    #print(root_file_path, mean_hi/len(print_dir_hi), mean_lo/len(print_dir_lo), (mean_lo/len(print_dir_lo)+mean_hi/len(print_dir_hi))/2)


    #print(result_dict_central)
    return result_dict_central, result_dict_hi, result_dict_lo