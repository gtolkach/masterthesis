#!/bin/bash
#python TemplateMaker.py TemlateMaker "HERAPDF20_NNLO_EIG" &
#python TemplateMaker.py TemlateMaker "HERAPDF20_A4_A9" &
#python TemplateMaker.py TemlateMaker "HERAPDF20_A4_A9SF10" &
#python TemplateMaker.py TemlateMaker "HERAPDF20_A4" &
#python TemplateMaker.py TemlateMaker "HERAPDF20_A4SF10" &
#python TemplateMaker.py TemlateMaker "HERAPDF20_A9" &
#python TemplateMaker.py TemlateMaker "HERAPDF20_A9SF10" &

#python TemplateMaker.py TemlateMaker "CT18NNLO" &
#python TemplateMaker.py TemlateMaker "CT18NNLO_A4" &
#python TemplateMaker.py TemlateMaker "CT18NNLO_A9" &
#python TemplateMaker.py TemlateMaker "CT18NNLO_A4_A9" &
#python TemplateMaker.py TemlateMaker "CT18NNLO_A4SF10" &
#python TemplateMaker.py TemlateMaker "CT18NNLO_A9SF10" &
#python TemplateMaker.py TemlateMaker "CT18NNLO_A4_A9SF10" &

python TemplateMaker.py TemlateMaker "NNPDF40_nnlo_as_01180_hessian" &
#python TemplateMaker.py TemlateMaker "NNPDF40_A4" &
#python TemplateMaker.py TemlateMaker "NNPDF40_A9" &
#python TemplateMaker.py TemlateMaker "NNPDF40_A4_A9" &
#python TemplateMaker.py TemlateMaker "NNPDF40_A4SF10" &
#python TemplateMaker.py TemlateMaker "NNPDF40_A9SF10" &
#python TemplateMaker.py TemlateMaker "NNPDF40_A4_A9SF10" &

#python TemplateMaker.py TemlateMaker "MSHT20nnlo_as118" &
#python TemplateMaker.py TemlateMaker "MSHT20_A4" &
#python TemplateMaker.py TemlateMaker "MSHT20_A9" &
#python TemplateMaker.py TemlateMaker "MSHT20_A4_A9" &
#python TemplateMaker.py TemlateMaker "MSHT20_A4SF10" &
#python TemplateMaker.py TemlateMaker "MSHT20_A9SF10" &
#python TemplateMaker.py TemlateMaker "MSHT20_A4_A9SF10" &

#python TemplateMaker.py TemlateMaker "CT18NNLO_A4_A9" &
#python TemplateMaker.py TemlateMaker "CT18NNLO_A4_A9SF10" &
#python TemplateMaker.py TemlateMaker "HERAPDF20_NNLO_EIG_A4_A9" &
#python TemplateMaker.py TemlateMaker "HERAPDF20_NNLO_EIG_A4_A9SF10" &
#python TemplateMaker.py TemlateMaker "MSHT20nnlo_as118_A4_A9" &
#python TemplateMaker.py TemlateMaker "MSHT20nnlo_as118_A4_A9SF10" &
#python TemplateMaker.py TemlateMaker "NNPDF40_nnlo_as_01180_hessian_A4_A9" &
#python TemplateMaker.py TemlateMaker "NNPDF40_nnlo_as_01180_hessian_A4_A9SF10" &