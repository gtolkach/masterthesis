import ROOT
from CalculatePDFUncFromFile import  CalculatePDFUnc
def DrawRelErr(pdf_name="",var="",channel=""):
    hist_name = "h_"+var
    #pdf_name = "NNPDF40"
    central, hi, lo = CalculatePDFUnc("rootfiles/"+channel+"_100k_"+pdf_name+".root",hist_name, pdf_name)
    central_Ai, hi_Ai, lo_Ai = CalculatePDFUnc("rootfiles/"+channel+"_100k_"+pdf_name+"_A4_A9.root",hist_name, pdf_name)
    central_AiSF10, hi_AiSF10, lo_AiSF10 = CalculatePDFUnc("rootfiles/"+channel+"_100k_"+pdf_name+"_A4_A9SF10.root",hist_name, pdf_name)

    delta = 5 
    if "mt" in hist_name:
        h_central = ROOT.TH1D("h_mt_central","; m_{T}; N",25 ,  50 , 100)
        delta *= (100 - 50)/25
    elif "pt" in hist_name:
        h_central = ROOT.TH1D("h_pt_central","; p_{T}; N",45 ,  0 , 90)
        delta *= (90 - 0)/45
    lumi = 1.0
    delta=delta*lumi
    print(h_central)
    h_pdf_error = ROOT.TGraphAsymmErrors(h_central)
    h_pdf_error_Ai = ROOT.TGraphAsymmErrors(h_central)
    h_pdf_error_AiSF10 = ROOT.TGraphAsymmErrors(h_central)

    n_bins = h_central.GetNbinsX()

    for i in range(n_bins):
        h_central.SetBinContent(i+1, central[i+1]/central[i+1])
        h_pdf_error.SetPointY(i, central[i+1]/central[i+1])
        h_pdf_error.SetPointEYlow(i, lo[i+1]/central[i+1])
        h_pdf_error.SetPointEYhigh(i, hi[i+1]/central[i+1])
        h_pdf_error_Ai.SetPointY(i, central_Ai[i+1]/central_Ai[i+1])
        h_pdf_error_Ai.SetPointEYlow(i, lo_Ai[i+1]/central_Ai[i+1])
        h_pdf_error_Ai.SetPointEYhigh(i, hi_Ai[i+1]/central_Ai[i+1])
        h_pdf_error_AiSF10.SetPointY(i, central_AiSF10[i+1]/central_AiSF10[i+1])
        h_pdf_error_AiSF10.SetPointEYlow(i, lo_AiSF10[i+1]/central_AiSF10[i+1])
        h_pdf_error_AiSF10.SetPointEYhigh(i, hi_AiSF10[i+1]/central_AiSF10[i+1])

    print(h_central.Integral())
    mg = ROOT.TMultiGraph()
    h_pdf_error.SetFillColorAlpha(ROOT.kBlue,0.6)
    mg.Add(h_pdf_error)
    tmp = h_pdf_error_Ai.Clone("tmpCLONE")
    tmp.SetFillColor(ROOT.kWhite)
    mg.Add(tmp)
    h_pdf_error_Ai.SetFillColorAlpha(ROOT.kRed,0.6)
    #h_pdf_error_Ai.SetFillStyle(3002)
    mg.Add(h_pdf_error_Ai)
    tmp2 = h_pdf_error_AiSF10.Clone("CLone2")
    tmp2.SetFillColor(ROOT.kWhite)
    mg.Add(tmp2)
    h_pdf_error_AiSF10.SetFillColorAlpha(ROOT.kGreen+2,0.6)
    mg.Add(h_pdf_error_AiSF10)







    c = ROOT.TCanvas()
    #h_central.Draw("hist")

    ROOT.gStyle.SetOptStat(0)
    legend = ROOT.TLegend(0.5, 0.75,0.7,0.88,"", "NDC")
    legend.SetFillStyle(0)
    legend.SetTextSize(0.03)
    legend.SetBorderSize(0)
    legend.SetFillColor(0)
    legend.SetTextFont(42)
    pdf_name_legend = ""
    if "HERA" in pdf_name:
        pdf_name_legend = "HERAPDF2.0NNLO"
        print(pdf_name_legend)
    elif "MSHT" in pdf_name:
        pdf_name_legend = "MSHT20NNLO"
    elif "NNPDF" in pdf_name:
        pdf_name_legend = "NNPDF4.0NNLO"
    else:
        pdf_name_legend = pdf_name

    legend.AddEntry(h_pdf_error, pdf_name_legend, "f")
    legend.AddEntry(h_pdf_error_Ai, pdf_name_legend+" + A_{4} + A_{9}","f")
    legend.AddEntry(h_pdf_error_AiSF10, pdf_name_legend+" + A_{4} + A_{9} (sf = 10)","f")



    h_central.SetLineColor(1)
    mg.GetYaxis().SetTitleOffset(1.4)

    if "mt" in hist_name:
        mg.GetXaxis().SetTitle("m_{T} [GeV]")
        mg.GetYaxis().SetTitle("Relative \Delta PDF")
        mg.GetXaxis().SetRangeUser(50,100)
        #mg.GetXaxis().SetRangeUser(0.0,90000)
        if "WPenu" in channel:
            if "CT18" in pdf_name:
                mg.GetYaxis().SetRangeUser(0.95,1.04)
            elif "HERA" in pdf_name:
                mg.GetYaxis().SetRangeUser(0.98,1.027)
            elif "NNPDF" in pdf_name:
                mg.GetYaxis().SetRangeUser(0.99,1.015)
            elif "MSHT" in pdf_name:
                mg.GetYaxis().SetRangeUser(0.98,1.025)
        else:
            if "CT18" in pdf_name:
                mg.GetYaxis().SetRangeUser(0.95,1.04)
            elif "HERA" in pdf_name:
                mg.GetYaxis().SetRangeUser(0.98,1.022)
            elif "NNPDF" in pdf_name:
                mg.GetYaxis().SetRangeUser(0.99,1.015)
            elif "MSHT" in pdf_name:
                mg.GetYaxis().SetRangeUser(0.98,1.025)



    if "pt" in hist_name:
        mg.GetXaxis().SetTitle("p_{T}^{l#nu} [GeV]")
        mg.GetYaxis().SetTitle("Relative \Delta PDF")
        mg.GetXaxis().SetRangeUser(0,90)  
        if "WPenu" in channel:
            if "CT18" in pdf_name:
                mg.GetYaxis().SetRangeUser(0.95,1.04)
            elif "HERA" in pdf_name:
                mg.GetYaxis().SetRangeUser(0.98,1.025)
            elif "NNPDF" in pdf_name:
                mg.GetYaxis().SetRangeUser(0.985,1.020)
            elif "MSHT" in pdf_name:
                mg.GetYaxis().SetRangeUser(0.975,1.03)
        else:
            if "CT18" in pdf_name:
                mg.GetYaxis().SetRangeUser(0.945,1.055)
            elif "HERA" in pdf_name:
                mg.GetYaxis().SetRangeUser(0.98,1.02)
            elif "NNPDF" in pdf_name:
                mg.GetYaxis().SetRangeUser(0.99,1.015)
            elif "MSHT" in pdf_name:
                mg.GetYaxis().SetRangeUser(0.98,1.025)
    pt =  ROOT.TPaveText(0.13,0.83,0.35,0.88,"NDC")
    pt.SetFillColor(0)
    pt.SetTextAlign(12)
    #pt.AddText('#it{ATLAS} #bf{#bf{Work in Progress}}')
    if "WMenu" in channel:
        pt.AddText('#bf{#bf{W^{-}#rightarrow e^{-}#nu, #sqrt{S}=13 TeV}}')
    elif "WPenu" in channel:
        pt.AddText('#bf{#bf{W^{+}#rightarrow e^{+}#nu, #sqrt{S}=13 TeV}}')

    mg.Draw("a4")
    pt.Draw("'NDC' same")


    #h_pdf_error_Ai.Draw("a2 same")

    #h_central.Draw("hist same")
    c.RedrawAxis()




    c.Update()
    #mg.GetXaxis().SetNoExponent(0)
    legend.Draw("same")
    c.Draw()
    fig_name = pdf_name+"_"+"errors_bands"+hist_name.split("h")[1]
    out_dir = "plots/"
    c.SaveAs(out_dir+channel+"_"+fig_name+'.jpg')
    c.SaveAs(out_dir+channel+"_"+fig_name+'.pdf')

channel = "WPenu"
DrawRelErr("NNPDF40","pt",channel)
DrawRelErr("HERAPDF20","pt",channel)
DrawRelErr("MSHT20","pt",channel)
DrawRelErr("CT18NNLO","pt",channel)

DrawRelErr("NNPDF40","mt",channel)
DrawRelErr("HERAPDF20","mt",channel)
DrawRelErr("MSHT20","mt",channel)
DrawRelErr("CT18NNLO","mt",channel)




