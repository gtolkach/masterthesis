import ROOT, lhapdf
from PDFTool import PDFTool
import math
import sys

from tqdm import tqdm
def TemlateMaker(PDF_set=""):
    if PDF_set == "":
        print("ERROR: There is no pdf set")
        exit(0)

    #PDF_set = "HERAPDF20_NNLO_EIG"
    #WMinus
    #file_path = "/eos/user/d/dponomar/Storage/Science/Wai/microtree_example/PDF_reweight/v20220825/mc16_13TeV.361103.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wminusenu.STDM4.e3601_s3126_r10244+r11165_p3665.root"
    file_path = "/eos/user/d/dponomar/Storage/Science/Wai/microtree_example/PDF_reweight/v20220718/mc16_13TeV.361100.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Wplusenu.e3601_s3126_r10244+r11165_p3665.root"

    file_in = ROOT.TFile(file_path)
    tree = file_in.Get("MicroTree/HWWTree_ee")
    pdf_tool_dict = {}
    pdf_sets = {"CT10":52, "CT18NNLO":58, "HERAPDF20":28, "MSHT20":64, "NNPDF40":50}

    n_variation = 0
    for key in pdf_sets:
        if key in PDF_set:
            print("PDF SET: ",key)
            n_variation = pdf_sets[key]



    hist_mt_dict = {}
    hist_pt_dict = {}

    for i in range(n_variation+1):
        if i == 0:
            pdf_tool_dict["PDF%s"%i] = PDFTool(PDF_set)
            hist_mt_dict["PDF%s"%i] = ROOT.TH1D("h_mt_central","; M_{T}; N",25 ,  50 , 100)
            hist_pt_dict["PDF%s"%i] = ROOT.TH1D("h_pt_central","; p_{T}; N",45 ,  0 , 90)

        else:
            pdftool = PDFTool(PDF_set,"PDF%s"%i)
            pdf_tool_dict["PDF%s"%i] = pdftool 
            hist_mt_dict["PDF%s"%i] = ROOT.TH1D("h_mt_%s"%pdftool.branchName,"; M_{T}; N",25 ,  50 , 100)
            hist_pt_dict["PDF%s"%i] = ROOT.TH1D("h_pt_%s"%pdftool.branchName,"; p_{T}; N",45 ,  0 , 90)

    leptonP4 = ROOT.TLorentzVector()
    metP4 = ROOT.TLorentzVector()

    h_weights = file_in.Get("MicroTree/lumiNorm")
    lumi = 335.18
    n_event = 100000
    n_event_in_tree = tree.GetEntries()
    ev_sf = n_event_in_tree/n_event
    sf = ev_sf*h_weights.GetBinContent(1)/h_weights.GetEntries()

    for i in tqdm(range(n_event)):
        tree.GetEntry(i)
        mt = (2*tree.lepPt0/1000*tree.lepPt1/1000*(1-math.cos(tree.lepPhi0 - tree.lepPhi1)))**(0.5)

        x_1 = tree.m_mcevt_pdf_x1
        x_2 = tree.m_mcevt_pdf_x2
        id_1 = tree.m_mcevt_pdf_id1
        id_2 = tree.m_mcevt_pdf_id2
        Q_scale = tree.m_mcevt_pdf_scale
        if mt< 50: continue
        if tree.lepPt0/1000< 25: continue
        if tree.lepPt1/1000< 25: continue
        if tree.lepTopoetcone20/tree.lepPt0>0.05:continue
        if tree.lepPtvarcone20/tree.lepPt0>0.1:continue

        #hist_mt_dict["PDF0"].Fill(mt)
        leptonP4.SetPtEtaPhiM(tree.lepPt0,tree.lepEta0,tree.lepPhi0,0.000510998910*1000.)
        metP4.SetPtEtaPhiM(tree.lepPt1,tree.lepEta1,tree.lepPhi1,0)
        bosonWP4 = leptonP4+metP4
        pt_Wboson = bosonWP4.Pt()/1000
        w_evnt = tree.mcEventWeight
        for i in range(n_variation+1):
            w = pdf_tool_dict["PDF%s"%i].GetPDFWeight(id_1, id_2, x_1,x_1, Q_scale)
            #w=w*w_evnt*sf
            hist_mt_dict["PDF%s"%i].Fill(mt,w)
            hist_pt_dict["PDF%s"%i].Fill(pt_Wboson,w)
            #print(w)

    if "NNPDF40_nnlo_as_01180_hessian" in PDF_set:
        PDF_set = "NNPDF40"
    file_path_out = "rootfiles/WPenu_100к_"+ PDF_set+".root"
    file_out = ROOT.TFile(file_path_out,"RECREATE")
    file_out.cd()
    for i in range(n_variation+1):
         hist_mt_dict["PDF%s"%i].Write()
         hist_pt_dict["PDF%s"%i].Write()
    file_out.Write()
    file_out.Close()

#TemlateMaker("HERAPDF20_NNLO_EIG")
#TemlateMaker("HERAPDF20_A4_A9")
#TemlateMaker("HERAPDF20_A4_A9SF10")
#TemlateMaker("HERAPDF20_A4")
#TemlateMaker("HERAPDF20_A9")
#TemlateMaker("HERAPDF20_A4SF10")
#TemlateMaker("HERAPDF20_A9SF10")
#TemlateMaker("MSHT20nnlo_as118")
#TemlateMaker("MSHT20_A4")
#TemlateMaker("MSHT20_A9")
#TemlateMaker("MSHT20_A4_A9")
#TemlateMaker("MSHT20_A4SF10")
#TemlateMaker("MSHT20_A9SF10")
#TemlateMaker("MSHT20_A4_A9SF10")

#TemlateMaker("NNPDF40_nnlo_as_01180_hessian")
#TemlateMaker("NNPDF40_A4")
#TemlateMaker("NNPDF40_A9")
#TemlateMaker("NNPDF40_A4_A9")
#TemlateMaker("NNPDF40_A4SF10")
#TemlateMaker("NNPDF40_A9SF10")
#TemlateMaker("NNPDF40_A4_A9SF10")
if __name__ == '__main__':
    args = sys.argv[-1]
    print(args)
    TemlateMaker(args)