setupATLAS
#export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
#source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
#lsetup "lcgenv -p LCG_96 x86_64-centos7-gcc62-opt lhapdf 6.2.3" # for PDF support
#lsetup "root 6.20.06-x86_64-centos7-gcc8-opt"
lsetup "views LCG_102 x86_64-centos7-gcc8-opt"
lsetup "lcgenv -p LCG_102 x86_64-centos7-gcc8-opt lhapdf 6.2.3" # for PDF support

#lsetup "root 6.08.06-x86_64-slc6-gcc62-opt"
#export LHAPDF_DATA_PATH=/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/:$LHAPDF__HOME/share/LHAPDF/
export LHAPDF_DATA_PATH=/afs/cern.ch/work/g/gtolkach/application/masterthesis/lhapdfsets/:$LHAPDF__HOME/share/LHAPDF/
lsetup git
#export PATH=${PATH}:/afs/cern.ch/work/g/gtolkach/application/masterthesis/external/fastjet-install/bin/